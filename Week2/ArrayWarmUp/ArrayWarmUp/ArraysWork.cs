﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayWarmUp
{
    public class ArraysWork
    {
        public bool FirstLast6(int[] numbers)
        {
            if (numbers.Length > 0)
                return (numbers[0] == 6 || numbers[numbers.Length - 1] == 6);
            return false;
        }

        public bool SameFirstLast(int[] numbers)
        {
            return (numbers.Length > 0 && numbers[0] == numbers[numbers.Length - 1]);
        }

        public int[] MakePi(int n)
        {
            int[] intArray = new int[n];
            string Pi = Math.PI.ToString();
            Pi = Pi.Remove(1, 1);

            for (int i = 0; i < n; i++)
            {
                intArray[i] = int.Parse(Pi[i].ToString());
            }
            return intArray;
        }

        public bool CommonEnd(int[] a, int[] b)
        {
            if (a.Length > 0 && b.Length > 0)
            {
                return (a[0] == b[0] || a[a.Length - 1] == b[b.Length - 1]);
            }
            return false;
        }

        public int Sum(int[] n)
        {
            int sum = 0;
            foreach (int num in n)
            {
                sum += num;
            }
            return sum;
        }

        public int[] RotateLeft(int[] numbers)
        {
            int[] num = new int[numbers.Length];

            num[num.Length - 1] = numbers[0];
            for (int i = 0; i <= numbers.Length - 2; i++)
            {
                num[i] = numbers[i + 1];

            }

            return num;
        }

        public int[] Reverse(int[] numbers)
        {
            int[] num = new int[numbers.Length];
            int counter = numbers.Length - 1;
            foreach (int n in numbers)
            {
                num[counter] = n;
                counter--;
            }

            return num;

        }

        public int[] HigherWin(int[] numbers)
        {
            int high = 0;
            int[] highest = new int[numbers.Length];

            if (numbers[0] > numbers[numbers.Length - 1])
                high = numbers[0];

            else if (numbers[0] < numbers[numbers.Length - 1])
                high = numbers[numbers.Length - 1];

            for (int i = 0; i < numbers.Length; i++)
                highest[i] = high;
            return highest;
        }

        public int[] GetMiddle(int[] a, int[] b)
        {
            int[] newArray = new int[2];

            newArray[0] = a[a.Length / 2];
            newArray[1] = b[b.Length / 2];

            return newArray;
        }

        public bool HasEven(int[] numbers)
        {
            foreach (int n in numbers)
            {
                return (n%2 == 0);
            }
            return false;
        }

        public int[] KeepLast(int[] numbers)
        {
            int[] newArray = new int[numbers.Length * 2];
            newArray[newArray.Length - 1] = numbers[numbers.Length - 1];

            return newArray;
        }

        public bool Double23(int[] numbers)
        {
            int counter2 = 0;
            int counter3 = 0;
            bool test = false;
            foreach (int n in numbers)
            {
                if (n == 2)
                    counter2++;
                else if (n == 3)
                    counter3++;
                if (counter2 == 2 || counter3 == 2)
                    test = true;
                else
                {
                    test = false;
                }     
            }
            return test;
        }

        public int[] Fix23(int[] numbers)
        {
            int[] num = numbers;
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if (numbers[i] == 2 && numbers[i + 1] == 3)
                {
                    num[i + 1] = 0;
                }
            }
            return num;
        }

        public bool Unlucky1(int[] numbers)
        {
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if (i < 2 || i > numbers.Length - 2)
                {
                    if (numbers[i] == 1 && numbers[i + 1] == 3)
                    {
                        return true;
                    }                   
                }             
            }
            return false;
        }

        public int[] Make2(int[] a, int[] b)
        {
           // int[] newArray = new int[a.Length + b.Length];
            int[] newArray = a.Concat(b).ToArray();
            int[] array = new int[2];
            int start = 0;

            for (int i = 0; i < 2; i++)
            {
                array[i] = newArray[i];
            }
            return array;
        }
    }
    }

