﻿using System;

namespace DateTimeLab
{
    class Program
    {
        static void Main(string[] args)
        {
             GetDateTime();
        }

        public static void GetDateTime()
        {
            DateTime date = new DateTime();
            string userInput = "";
            string amountInput = "";
            int amount;

            Console.Write("Enter a date: ");
            userInput = Console.ReadLine();

            DateTime.TryParse(userInput, out date);
            Console.WriteLine("Date entered: {0}",date.ToString("MM/dd/yyyy"));
            
            Console.Write("How many Wednesday would you like to find? : ");
            amountInput = Console.ReadLine();

            int.TryParse(amountInput, out amount);
            Console.WriteLine("Dates for all the Wednesday!");

            for (int i = 0; i < amount * 7; i++)
            {
                CheckForWednesday(date);
                date = date.AddDays(1);
            }
            
            Console.ReadLine();
        }

        public static void CheckForWednesday(DateTime date)
        {
            DayOfWeek day = date.DayOfWeek;

            switch (day)
            {
                case DayOfWeek.Wednesday:
                    Console.WriteLine("{0}", date.ToString("MM/dd/yyyy"));
                    break;
                default:
                    break;
            }
        }
    }
}
