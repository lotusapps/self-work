﻿using System;
using System.Collections.Generic;
using TwoDiceLab.View;

namespace TwoDiceLab.WorkFlow
{
    class Logic
    {
        public void StartRollUsingArray()
        {
            Display _Display = new Display();
            int[] AmountOfRolls = new int[11];

            Random r = new Random();

            int Dice1 = 0;
            int Dice2 = 0;
            int Total = 0;

            for (int i = 0; i < 100; i++)
            {
                Dice1 = r.Next(1, 7);
                Dice2 = r.Next(1, 7);
                Total = Dice1 + Dice2;
                //_Display.DisplayRoll(Total);

                switch (Total)
                {
                    case 2:
                        AmountOfRolls[0] += 1;
                        break;
                    case 3:
                        AmountOfRolls[1] += 1;
                        break;
                    case 4:
                        AmountOfRolls[2] += 1;
                        break;
                    case 5:
                        AmountOfRolls[3] += 1;
                        break;
                    case 6:
                        AmountOfRolls[4] += 1;
                        break;
                    case 7:
                        AmountOfRolls[5] += 1;
                        break;
                    case 8:
                        AmountOfRolls[6] += 1;
                        break;
                    case 9:
                        AmountOfRolls[7] += 1;
                        break;
                    case 10:
                        AmountOfRolls[8] += 1;
                        break;
                    case 11:
                        AmountOfRolls[9] += 1;
                        break;
                    case 12:
                        AmountOfRolls[10] += 1;
                        break;
                }
            }

            for (int i = 0; i < AmountOfRolls.Length; i++)
            {
                _Display.DisplayTotalRoll(i + 2, AmountOfRolls[i]);
            }
        }

        public void StartRollUsingDictionary()
        {
            Display _Display = new Display();

            Dictionary<int, int> dictionary = new Dictionary<int, int>()
            {
                {2, 0},
                {3, 0},
                {4, 0},
                {5, 0},
                {6, 0},
                {7, 0},
                {8, 0},
                {9, 0},
                {10, 0},
                {11, 0},
                {12, 0}
            };
            Random r = new Random();

            int Dice1 = 0;
            int Dice2 = 0;
            int Total = 0;

            for (int i = 0; i < 100; i++)
            {
                Dice1 = r.Next(1, 7);
                Dice2 = r.Next(1, 7);
                Total = Dice1 + Dice2;

                dictionary[Total]++;
            }
            for (int i = 0; i < dictionary.Count; i++)
            {
                _Display.DisplayTotalRoll(i + 2, dictionary[i + 2]);
            }
            
        }
    }
}
