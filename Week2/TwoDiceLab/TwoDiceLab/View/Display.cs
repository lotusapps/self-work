﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwoDiceLab.View
{
    class Display
    {
        public void DisplayRoll(int num)
        {
            Console.WriteLine("{0}", num);
        }

        public void DisplayTotalRoll(int num, int AmountOfTime)
        {
            Console.WriteLine("Number {0} was rolled {1} times.", num, AmountOfTime);
        }
    }
}
