﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwoDiceLab.WorkFlow;

namespace TwoDiceLab
{
    class Program
    {
        static void Main(string[] args)
        {
            Logic startLogic = new Logic();
            Console.WriteLine("Rolls using Arrays!");
            startLogic.StartRollUsingArray();
            
            Console.WriteLine("\nRolls using dictionary!");
            startLogic.StartRollUsingDictionary();
            Console.Read();
        }
    }
}
