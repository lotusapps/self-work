﻿using System.Collections.Generic;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.UI.Models
{
    public class NewPostList
    {
        public List<Post> Posts { get; set; }
        public int Total { get; set; }

        public NewPostList()
        {
            Posts = new List<Post>();
        }
    }
}