﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.UI.Models
{
    public class StaticPageVm
    {
        public StaticPage StaticPage { get; set; }
        public string StaticPageTitle { get; set; }
        public string StaticPageContent { get; set; }
    }
}