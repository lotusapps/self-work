﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MSRM.Capstone.UI.Startup))]
namespace MSRM.Capstone.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
