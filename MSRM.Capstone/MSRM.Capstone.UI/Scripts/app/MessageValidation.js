﻿$(document).ready(function () {
    $('#SendMsgForm').validate({
        rules: {
            FirstName: {
                required: true
            },
            LastName: {
                required: true
            },
            EmailAddress: {
                required: true,
                email: true
            },
            MessageBody: {
                required: true
            },
            CaptchaCode: {
                required: true
            }
        },
        messages: {
            FirstName: "Please Enter Your First Name",
            LastName: "Please Enter Your Last Name",
            EmailAddress: "Please Enter a Valid E-Mail Address",
            MessageBody: "Please Enter a Message",
            CaptchaCode: "Please Fill Out the Captcha"
        },
        invalidHandler: function(event, validator) {
            console.log("Houston, we've had a problem.");
        }
});
});