﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MSRM.Capstone.BLL;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.UI.Controllers
{
    [Authorize(Roles = "SuperUser")]
    public class TagController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}