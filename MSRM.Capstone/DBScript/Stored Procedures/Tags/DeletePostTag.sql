USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[DeletePostTag]    Script Date: 3/22/2016 1:53:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[DeletePostTag](

	@PostId int,
	@TagId int

)AS

DELETE FROM PostTags
WHERE PostID = @PostId And TagID = @TagId




GO

