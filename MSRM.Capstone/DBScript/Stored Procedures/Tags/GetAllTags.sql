USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[GetAllTags]    Script Date: 3/10/2016 10:25:49 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[GetAllTags]
As

Select t.TagID, t.Tag AS Name
From Tags t


GO

