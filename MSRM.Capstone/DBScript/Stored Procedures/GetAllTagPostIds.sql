USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[GetAllTagPostIds]    Script Date: 3/23/2016 2:08:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE Procedure [dbo].[GetAllTagPostIds]
(
	@TagId int
)As


SELECT *
FROM PostTags pt
JOIN Posts p
ON pt.PostID = p.PostID
WHERE pt.TagID = @TagId
AND p.StatusID = 4

GO

