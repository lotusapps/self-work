USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[AddPostCategories]    Script Date: 3/14/2016 10:45:12 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[AddPostCategories]
(
    @PostId int,
	@CategoryId int 
) AS

INSERT INTO PostCategories
VALUES(@PostId, @CategoryId)

GO

