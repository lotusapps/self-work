USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[AddCategory]    Script Date: 3/10/2016 11:05:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[AddCategory]
(
    @CategoryName nvarchar(50),
	@CategoryId int output
) AS

INSERT INTO Categories
VALUES (@CategoryName)

SET @CategoryId = SCOPE_IDENTITY();

GO

