USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[AddToUncategorized]    Script Date: 3/11/2016 10:38:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[AddToUncategorized]
AS

INSERT INTO PostCategories (PostID,CategoryID)
SELECT p.PostID, 1
FROM Posts p
LEFT JOIN PostCategories pc
ON p.PostID = pc.PostID
GROUP BY p.PostID
HAVING Count(pc.CategoryID) = 0


GO

