USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[DeleteCategory]    Script Date: 3/10/2016 3:46:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[DeleteCategory]
(
    @CategoryId int
) AS

DELETE FROM PostCategories WHERE CategoryID = @CategoryId
DELETE FROM Categories WHERE CategoryID = @CategoryId
GO

