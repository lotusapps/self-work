USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[GetOrderedInquiryList]    Script Date: 3/23/2016 3:46:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[GetOrderedInquiryList] AS

Select i.InquiryID AS MessageId, i.MessageStatus AS isRead, i.FirstName, i.LastName, i.InquiryDate AS [Date], i.Inquiry AS MessageBody, i.EmailAddress
From Inquiries i
Order By i.MessageStatus, i.InquiryDate DESC


GO

