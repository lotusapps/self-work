﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSRM.Capstone.Model
{
    public class Message
    {
        public int MessageId { get; set; }
        public byte isRead { get; set; }

        [Required(ErrorMessage = "Please Enter your First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please Enter your Last Name")]
        public string LastName { get; set; }
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "Please Enter a Message")]
        public string MessageBody { get; set; }

        [Required(ErrorMessage = "Please Enter an E-Mail")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        public string EmailAddress { get; set; }

        public Message()
        {
            isRead = 0;
            Date = DateTime.Today;
        }

        public string IsReadConvert()
        {
            if (isRead == 0)
                return "Unread"; //Message Read
            else
                return "Read"; //Message Unread
        }

        public string DateString => this.Date.ToString("D");

        public string GetDateString()
        {
            return this.Date.ToString("D");
        }
    }
}
