﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSRM.Capstone.Model
{
    public class User
    {
        public string UserEmail { get; set; }
        public string UserName { get; set; }
        public int RoleId { get; set; }
    }
}
