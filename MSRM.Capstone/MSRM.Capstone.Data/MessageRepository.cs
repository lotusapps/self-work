﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.Data
{
    public class MessageRepository
    {
        public List<Message> _messageList { get; set; }

        public MessageRepository()
        {
            _messageList = Load();
        }

        private List<Message> Load()
        {
            using (SqlConnection cn = new SqlConnection(Settings.Config.ConnectionString))
            {
                var messageList = cn.Query<Message>("GetOrderedInquiryList",
                    commandType: CommandType.StoredProcedure).ToList();

                return messageList;
            }
        }

        public Message GetMessageFromId(int msgId)
        {
            var m = new DynamicParameters();
            m.Add("msgId", msgId);

            using (SqlConnection cn = new SqlConnection(Settings.Config.ConnectionString))
            {
                var msg = cn.Query<Message>("GetAnInquiryById", m,
                    commandType: CommandType.StoredProcedure).FirstOrDefault();

                return msg;
            }
        }

        public int CreateMessage(Message newMsg)
        {
            using (SqlConnection cn = new SqlConnection(Settings.Config.ConnectionString))
            {
                var m = new DynamicParameters();
                m.Add("InquiryId", DbType.Int32, direction: ParameterDirection.Output);
                m.Add("Status", newMsg.isRead);
                m.Add("FirstName", newMsg.FirstName);
                m.Add("LastName", newMsg.LastName);
                m.Add("Date", newMsg.Date);
                m.Add("Message", newMsg.MessageBody);
                m.Add("EmailAddress",newMsg.EmailAddress);

                cn.Execute("CreateInquiry", m,
                    commandType: CommandType.StoredProcedure);
                int msgId = m.Get<int>("InquiryId");
                newMsg.MessageId = msgId;

                return msgId;
            }
        }

        public void DeleteMessage(int msgId)
        {
            var m = new DynamicParameters();
            m.Add("msgId", msgId);

            using (SqlConnection cn = new SqlConnection(Settings.Config.ConnectionString))
            {
                cn.Execute("DeleteInquiry", m,
                    commandType: CommandType.StoredProcedure);
            }

            _messageList = Load();
        }

        public void UpdateMessage(Message updateMsg)
        {
            using (SqlConnection cn = new SqlConnection(Settings.Config.ConnectionString))
            {
                var m = new DynamicParameters();
                m.Add("InquiryId", updateMsg.MessageId);
                m.Add("Status", updateMsg.isRead);

                cn.Execute("UpdateInquiryStatus", m,
                    commandType: CommandType.StoredProcedure);

                _messageList = Load();
            }
        }
    }
}
