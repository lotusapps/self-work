Feature: CreateTag
	In order to add new tags (keywords) for post content
	As an user or superuser
	I want to create a new tag keyword in the database

#@mytag
#Scenario: Create a tag
#	Given I have entered the tag keyword in the Tag Manager form
#	When I press submit
#	Then the result should be a new tag keyword has been added to the database


Feature: DeleteTag
	In order to manage tags within the content management system (cms)
	As an user or superuser
	I want to delete a tag keyword from the database

#@mytag
#Scenario: Delete a tag
#	Given I have selected a tag keyword from a list of tags retrieved from the database
#	When I press delete
#	Then the result should be the tag keyword has been from the database
