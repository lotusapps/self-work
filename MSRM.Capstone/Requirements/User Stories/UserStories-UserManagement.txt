Feature: CreateUser
	In order to add new users
	As an administrator
	I want to create a new user and assign a role in the database

#@mytag
#Scenario: Create a user
#	Given I have entered the user's first name
#	And I have entered the user's last name
#	And I have entered the user's email address
#	And I have selected the user's role
#	When I press submit
#	Then the result should be a new user added to the database


Feature: DisableUser
	In order to manage user access within the content management system (cms)
	As an administrator
	I want to delete a user from the database

#@mytag
#Scenario: Disable a user account
#	Given I have retrieved a user record from the database
#	And I have selected "disable user"
#	When I press submit
#	Then the result should be the user role has been changed to disable in the database
