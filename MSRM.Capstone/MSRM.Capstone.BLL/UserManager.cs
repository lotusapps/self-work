﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSRM.Capstone.Data;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.BLL
{
    public class UserManager
    {
        public List<User> GetAllUsers()
        {
            var repo = new UserRepository();
            return repo.GetAllUsers();
        }

        public void ChangeUserRole(string email, int role)
        {
            var repo = new UserRepository();
            repo.ChangeUserRole(email, role);
        }

        public void AlterUserInfo(User user)
        {
            var repo = new UserRepository();
            repo.AlterUserInfo(user);
        }
    }
}
