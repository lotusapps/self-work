﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSRM.Capstone.Data;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.BLL
{  
    public class TagManager
    {
        private TagRepository _repo { get; set; }

        public TagManager()
        {
            _repo = new TagRepository();
        }

        public Response<Tag> AddTag(Tag tagToAdd)
        {
            Response<Tag> response = new Response<Tag>();

            try
            {
                _repo.AddTag(tagToAdd);
                if (_repo._tagList.Any(t => t.Name == tagToAdd.Name))
                {
                    response.Success = true;
                    response.Message = "You have added a tag.";
                    response.Data = tagToAdd;
                }
                else
                {
                    response.Success = false;
                    response.Message = "Tag could not be added. Please Try Again.";
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }

        //Check for Tag and Create if Tag does not exist.
        public Response<Tag> CheckForTag(Tag checkTag)
        {
            Response<Tag> response = new Response<Tag>();

            var tag = _repo.GetTagFromName(checkTag.Name);

            if (tag == null)
            {
                response = AddTag(checkTag);
            }
            else
            {
                response.Data = tag;
                response.Message = "Tag Already Exists.";
            }

            return response;
        } 

        public Response<Tag> GetATagFromName(string tagName)
        {
            Response<Tag> response = new Response<Tag>();

            try
            {
                Tag tag = _repo.GetTagFromName(tagName);
                if (tag != null)
                {
                    response.Success = true;
                    response.Data = tag;
                }
                else
                {
                    response.Success = false;
                    response.Message = "Unable to retrieve Tag.";
                    response.Data = tag;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<Tag> GetATagFromId(int tagId)
        {
           Response<Tag> response = new Response<Tag>();

            try
            {
               Tag tag = _repo.GetTagFromId(tagId);
                if (tag != null)
                {
                    response.Success = true;
                    response.Data = tag;
                }
                else
                {
                    response.Success = false;
                    response.Message = "Unable to retrieve Tag.";
                    response.Data = tag;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        } 

        public Response<List<Tag>> GetPostTagsfromPostId(int postId)
        {
            Response<List<Tag>> response = new Response<List<Tag>>();

            try
            {
                var tagList = _repo.GetTagsFromPostId(postId);
                if (tagList != null)
                {
                    response.Success = true;
                    response.Data = tagList;
                }
                else
                {
                    response.Success = false;
                    response.Message = "Unable to load TagList";
                    response.Data = tagList;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Response<List<Tag>> GetAllTags()
        {
            var response = new Response<List<Tag>>();

            try
            {
                List<Tag> tagList = _repo._tagList;
                if (tagList.Count != 0)
                {
                    response.Success = true;
                    response.Data = tagList;
                }
                else
                {
                    response.Success = false;
                    response.Message = "There are no tags.";
                    response.Data = null;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }

        public Dictionary<Tag, int> CountTags()
        {
            Dictionary<Tag, int> tagCount = new Dictionary<Tag, int>();

            foreach (var tag in _repo._tagList)
            {
                var total = _repo.GetPostIdsFromTagId(tag.TagId).Count;

                tagCount.Add(tag, total);
            }

            return tagCount;
        }

        public List<int> GetPostIdsByTag(Tag tag)
        {
            return _repo.GetPostIdsFromTagId(tag.TagId);
        } 

        public Response<Tag> DeleteTag(int tagId)
        {
            var response = new Response<Tag>();

            try
            {
                _repo.DeleteTag(tagId);
                if (_repo._tagList.Any(t => t.TagId == tagId))
                {
                    response.Success = false;
                    response.Message = "Error. Tag could not be deleted.";
                }
                else
                {
                    response.Success = true;
                    response.Message = "The tag was deleted.";
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        } 
    }
}
