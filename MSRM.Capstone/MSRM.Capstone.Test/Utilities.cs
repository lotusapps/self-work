﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSRM.Capstone.Data.Settings;

namespace MSRM.Capstone.Test
{
    internal static class Utilities
    {
        internal static void RebuildTestDb()
        {
            using (var cn = new SqlConnection(Config.ConnectionString))
            {
                var cmd = new SqlCommand()
                {
                    CommandText = new FileInfo("Scripts/RebuildTestDb.txt").OpenText().ReadToEnd(),
                    Connection = cn
                };
                cn.Open();

                cmd.ExecuteNonQuery();
            }
        }
    }
}
