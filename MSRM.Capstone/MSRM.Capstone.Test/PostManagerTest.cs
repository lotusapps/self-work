﻿using System;
using System.Collections.Generic;
using MSRM.Capstone.BLL;
using MSRM.Capstone.Data;
using MSRM.Capstone.Model;
using NUnit.Framework;

namespace MSRM.Capstone.Test
{
    [TestFixture]
    public class PostManagerTest
    {
        private PostManager _postManager;
        private Post _testPost;

        [SetUp]
        public void TestSetUp()
        {
            Utilities.RebuildTestDb();
            _postManager = new PostManager();
            _testPost = new Post();
        }

        [Test]
        public void CanGetAllPosts()
        {
            Response<List<Post>> actual = _postManager.GetAllPosts();

            Assert.AreEqual(18, actual.Data.Count);
        }

        [Test]
        public void CreatePostSuccess()
        {
            Post newPost = new Post
            {
                UserId = "fa8905d9-3c30-41a0-bc54-72a8c80cada8",
                StatusId = 1,
                PostDate = DateTime.Today,
                Posting = "Test File Post",
                Title = "Better work!",
                
            };

            var actual = _postManager.CreatePost(newPost);

            Assert.IsTrue(actual.Success);
        }

    }
}
