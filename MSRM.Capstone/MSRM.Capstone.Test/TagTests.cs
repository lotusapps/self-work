﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSRM.Capstone.BLL;
using MSRM.Capstone.Data;
using MSRM.Capstone.Model;
using NUnit.Framework;

namespace MSRM.Capstone.Test
{
    [TestFixture]
    public class TagTests
    {
        private TagRepository _tagRepo;
        private Tag _testTag;

        [SetUp]
        public void TestSetUp()
        {
            _tagRepo = new TagRepository();
            _testTag = new Tag();
            //Utilities.RebuildTestDb();
        }

        [Test] 
        public void CanGetAllTags()
        {
            List<Tag> listTags = _tagRepo._tagList;

            Assert.AreEqual(34, listTags.Count);
        }

        [Test]
        public void CanAddTag()
        {
            var manager = new TagManager();
            _testTag.Name = "CodingRocks";

            var response = manager.AddTag(_testTag);

            Assert.IsTrue(response.Success);
        }

        [Test]
        public void CanDeleteTag()
        {
            var manager = new TagManager();
            var response = manager.DeleteTag(4);

            Assert.IsTrue(response.Success);
        }

        [Test] 
        public void CanGetATag()
        {
            _testTag = _tagRepo.GetTagFromId(1);

            Assert.AreEqual("C#", _testTag.Name);
        }

        [Test]
        public void GetPostCount()
        {
            var manager = new TagManager();
            var response = manager.GetPostTagsfromPostId(2);

            Assert.AreEqual(1, response.Data.Count);
        }

        [Test]
        public void GetPostCountInvalidPostId()
        {
            var manager = new TagManager();
            var response = manager.GetPostTagsfromPostId(900);

            Assert.IsEmpty(response.Data);
        }
    }
}
