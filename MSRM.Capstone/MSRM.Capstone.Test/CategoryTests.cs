﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSRM.Capstone.Data;
using MSRM.Capstone.Model;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace MSRM.Capstone.Test
{
    [TestFixture]
    class CategoryTests
    {
        [SetUp]
        public void TestSetUp()
        {
            Utilities.RebuildTestDb();
        }

        [Test]
        public void GetAllTest()
        {
            CategoryRepository repo = new CategoryRepository();
            var actual = repo.GetAllCategories();
            Assert.AreEqual(19, actual.Count);
            Assert.AreEqual(".Net", actual.ToArray()[1].CategoryName);
        }

        [Test]
        public void GetPostCategoriesTest()
        {
            CategoryRepository repo = new CategoryRepository();
            var actual = repo.GetCategoriesByPostId(2);
            Assert.AreEqual(1,actual.Count);
        }

        [TestCase("Test", 40)]
        [TestCase("Events", 7)]
        public void AddCategoryTest(string input, int expected)
        {
            CategoryRepository repo = new CategoryRepository();
            int actual = repo.AddCategory(input);
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void AddPostCategoriesTest()
        {
            CategoryRepository repo = new CategoryRepository();
            List<string> categoryList = new List<string> {"Test", "Events", "ComputerStuff", ".Net"};
            List<int> actual = repo.AddCategoriesFromPost(categoryList, 23);
            Assert.AreEqual(40,actual.ToArray()[0]);
            Assert.AreEqual(2, actual.ToArray()[3]);
            int postCategories = repo.GetCategoriesByPostId(23).Count;
            Assert.AreEqual(4,postCategories);
        }

        [Test]
        public void DeleteCategoryTest()
        {
            CategoryRepository repo = new CategoryRepository();
            var list = repo.GetCategoriesByPostId(2);
            repo.DeleteCategory(3);
            var newList = repo.GetCategoriesByPostId(2);
            var uncategorized = repo.GetCategoriesByPostId(12);
            var categoryList = repo.GetAllCategories();
            Assert.AreEqual(18,categoryList.Count);
            Assert.AreEqual(1,list.Count);
            Assert.AreEqual(1,newList.Count);
            Assert.AreEqual(1,uncategorized.Count);
        }
    }
}
