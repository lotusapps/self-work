﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Constraints;

namespace Warmups.BLL
{
    public class Logic
    {
        /* When squirrels get together for a party, they like to have cigars. 
           A squirrel party is successful when the number of cigars is between 
           40 and 60, inclusive. Unless it is the weekend, in which case there is 
           no upper bound on the number of cigars. Return true if the party with 
           the given values is successful, or false otherwise. 
        */
        public bool GreatParty(int cigars, bool isWeekend)
        {
            if (isWeekend)
                return cigars > 40;
            else
                return (cigars >= 40 && cigars <= 60);
        }

        //CanHazTable
        public int CanHazTable(int yourStyle, int dateStyle)
        {
            int rating = 0;
            if (yourStyle >= 8 || dateStyle >= 8)
            {
                rating = 2;
            }
            else if (yourStyle <= 2 || dateStyle <= 2)
            {
                rating = 0;
            }
            else
            {
                rating = 1;
            }
            return rating;
        }

        //PlayOutside
        public bool PlayOutside(int temp, bool isSummer)
        {
            if (isSummer)
            {
                if(temp >= 60 && temp <= 100)
                    return true;
                return false;
            }
            else
            {
                if (temp >= 60 && temp <= 90)
                    return true;
                return false;
            }           
        }

        //CaughtSpeeding
        public int CaughtSpeeding(int speed, bool isBirthday)
        {
            int result = 0;
            switch (isBirthday)
            {
                case true:
                    if (speed <= 65)
                    {
                        result = 0;
                    }
                    else if (speed > 65 && speed <= 85)
                    {
                        result = 1;
                    }
                    else
                    {
                        result = 2;
                    }
                    break;
                case false:
                    if (speed <= 60)
                    {
                        result = 0;
                    }
                    else if (speed > 60 && speed <= 80)
                    {
                        result = 1;
                    }
                    else
                    {
                        result = 2;
                    }
                    break;
                default:
                    result = 0;
                    break;
            }
            return result;
        }

        //SkipSum
        public int SkipSum(int a, int b)
        {
            int sum = a + b;

            if (sum >= 10 && sum <= 19)
            {
                return 20;
            }
            else
            {
                return sum;
            }
        }

        //AlarmClock
        public string AlarmClock(int day, bool vacation)
        {
            string time = "";
            if (vacation)
            {
                switch (day)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        time = "10:00";
                        break;
                    case 6:
                    case 0:
                        time = "off";
                        break;
                }
            }
            else
            {
                switch (day)
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        time = "7:00";
                        break;
                    case 6:
                    case 0:
                        time = "10:00";
                        break;
                }
            }
            return time;
        }

        //LoveSix
        public bool LoveSix(int a, int b)
        {
            int sum = a + b;
            int difference = Math.Abs(a - b);

            return (sum == 6 || a == 6 || b == 6 || difference == 6);
        }

        //InRange
        public bool InRange(int n, bool outsideMode)
        {
            switch (outsideMode)
            {
                case true:
                    if (n <= 1 || n >= 10)
                        return true;
                    break;
                case false:
                    if (n >= 1 && n <= 10)
                        return true;
                    break;
            }
            return false;
        }

        //SpecialElevel
        public bool SpecialEleven(int n)
        {
            return (n % 11 <= 1);
        }

        //Mod20
        public bool Mod20(int n)
        {
            if (n < 0)
            {
                return false;
            }
            else if (n%20 == 1 || n%20 == 2)
            {
                return true;
            }
            return false;
        }

        //Mod35
        public bool Mod35(int n)
        {
            if (n < 0 || (n % 3 == 0 && n % 5 == 0))
            {
                return false;
            }
            else if(n % 3 == 0)
            {
                return true;
            }
            else if (n % 5 == 0)
            {
                return true;
            }
            return false;
        }

        //AnswerCell
        public bool AnswerCell(bool isMorning, bool isMom, bool isAsleep)
        {
            if (isMorning)
            {
                if (isMom)
                    return true;
                return false;
            }else if (isAsleep)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        //TwoIsOne
        public bool TwoIsOne(int a, int b, int c)
        {
            return (a + b == c || a + c == b || b + c == a);

        }

        //AreInOrder
        public bool AreInOrder(int a, int b, int c, bool OK)
        {
            if (b > a && c > b && !OK)
                return true;
            else if (OK && c > b)
                return true;
            return false;
        }

        //InOrderEqual
        public bool InOrderEqual(int a, int b, int c, bool equalOK)
        {
            if (a < b && b < c)
                return true;
            else if (equalOK && a <= b && b <= c)
                return true;
            else
            {
                return false;
            }
        }

        //LastDigit
        public bool LastDigit(int a, int b, int c)
        {
            return (a%10 == b%10 || b%10 == c%10 || a%10 == c%10);
        }

        //RollDice
        public int RollDice(int die1, int die2, bool noDoubles)
        {
            int sum = 0;
            switch (noDoubles)
            {
                case true:
                    if (die1 == 6 && die1 == die2)
                        die1 = 1;
                    else if(die1 == die2)
                    {
                        die1++;
                    }         
                    sum = die1 + die2;
                    break;
                case false:
                    sum = die1 +die2;
                    break;
            }

            return sum;
        }

    }
}

