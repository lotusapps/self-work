﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Conditionals
    {
        /* We have two children, a and b, and the parameters aSmile and 
           bSmile indicate if each is smiling. We are in trouble if they 
           are both smiling or if neither of them is smiling. Return true 
           if we are in trouble. 
        */
        public bool AreWeInTrouble(bool aSmile, bool bSmile)
        {
            if (aSmile && bSmile)
                return true;

            if (!aSmile && !bSmile)
                return true;

            return false;
        }

        //SleepingIn
        public bool CanSleepIn(bool isWeekday, bool isVacation)
        {
           return isVacation || !(isWeekday);
        }

        //SumDouble
        public int SumDouble(int a, int b)
        {
            int sum = (a == b) ? (2*(a + b)) : (a + b);    
            return sum;
        }

        //Diff21
        public int Diff21(int n)
        {
            int differ = n > 21 ? (n - 21)*2 : (n - 21);
            return Math.Abs(differ);
        }

        //ParrotTrouble
        public bool ParrotTrouble(bool isTalking, int hour)
        {
             return isTalking && (hour > 20 || hour < 7);
        }

        //Makes10
        public bool Makes10(int a, int b)
        {
            bool makes10;
            if (a == 10 || b == 10)
            {
                makes10 = true;
            }
            else if (a + b == 10)
            {
                makes10 = true;
            }
            else
            {
                makes10 = false;
            }
            return makes10;
        }

        //NearHundred
        public bool NearHundred(int n)
        {
            return Math.Abs(n - 100) <= 10 || Math.Abs(n - 200) <= 10;
        }

        //PosNeg
        public bool PosNeg(int a, int b, bool negative)
        {
            bool test = negative;

            if (a < 0 && b < 0 && negative)
            {
                test = true;
            }
            else if ((a < 0 || b < 0) && !negative)
            {
                test = true;
            }
                return test;
        }

        //NotString
        public string NotString(string s)
        {
            string notString = "not ";
            return (s.Length >= 3 && s.Substring(0, 3) == "not") ? s : string.Concat(notString, s);
        }

        //MissingChar
        public string MissingChar(string str, int n)
        {
            if (str.Length != 0 && n.ToString() != "")
            {
               return str.Remove(n, 1);
            }
           
            return str;
            
        }

        //FrontBack
        public string FrontBack(string str)
        {
            char[] letters = str.ToCharArray();
            char temp = letters[0];
            letters[0] = letters[letters.Length - 1];
            letters[letters.Length - 1] = temp;
            String word = new String(letters);

            return word;
        }

        //Front3
        public string Front3(string str)
        {
            if (str.Length >= 3)
            {
                return string.Format("{0}{0}{0}", str.Remove(3));
            }

            return string.Format("{0}{0}{0}", str);
        }

        //BackAround
        public string BackAround(string str)
        {
            if (str.Length > 0)
            {
                string lastLetter = str.Substring(str.Length - 1);
                return string.Format("{0}{1}{0}", lastLetter, str);
            }
            return str;
        }

        //Multiple3or5
        public bool Multiple3Or5(int number)
        {
            return number%3 == 0 || number%5 == 0;
        }

        //StartHi
        public bool StartHi(string str)
        {
            if (str.Length < 3)
            {
                return str.Contains("hi");
            }
            return str.Substring(0, 3) == "hi ";
        }

        //IcyHot
        public bool IcyHot(int temp1, int temp2)
        {
            return ((temp1 < 0 && temp2 > 100) || (temp1 > 100 && temp2 < 0)) ;
        }

        //Between10And20
        public bool Between10And20(int a, int b)
        {
            return (a > 10 && a < 20) || (b > 10 && b < 20) ;
        }

        //HasTeen
        public bool HasTeen(int a, int b, int c)
        {
            return (a >= 13 && a <= 19) || (b >= 13 && b <= 19) || (c >= 13 && c <= 19);
        }

        //SoAlone
        public bool SoAlone(int a, int b)
        {
            if (a >= 13 && a <= 19 && b >= 13 && b <= 19)
            {
                return false;
            }else if (a >= 13 && a <= 19 || b >= 13 && b <= 19)
            {
                return true;
            }
            return false;
        }

        //RemoveDel
        public string RemoveDel(string str)
        {
            if (str.Substring(1, 3).Contains("del"))
            {
                return str.Remove(1, 3);
            }
            return str;
        }

        //IxStart
        public bool IxStart(string str)
        {
            if (str.Substring(1, 2).Contains("ix"))
            {
                return true;
            }
            return false;
        }

        //StartOz
        public string StartOz(string str)
        {
            if (str.Length >= 2)
            {
                string word = str.Substring(0, 2);

                if (word.Contains("oz"))
                {
                    return word;
                }
                else if (word[0] == 'o')
                {
                    return word.Remove(1);
                }
                else if (word[1] == 'z')
                {
                    return word.Remove(0, 1);
                }
            }
            return str;
        }

        //Max
        public int Max(int a, int b, int c)
        {
            int largest = a;
            int[] array = new[] {a, b, c};
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] > largest)
                    largest = array[i];
            }
            return largest;
        }

        //Closer
        public int Closer(int a, int b)
        {
            int testA = Math.Abs(10 - a);
            int testB = Math.Abs(10 - b);
            int closest = 0;

            if (testA < testB)
            {
                closest = a;
            }
            else if (testB < testA)
            {
                closest = b;
            }
            else
            {
                return closest;
            }
            return closest;
        }

        //GotE
        public bool GotE(string str)
        {
            int eCount = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == 'e')
                    eCount++;
            }
            if (eCount <= 3 && eCount >= 1)
                return true;
            return false;
        }

        //EndUp
        public string EndUp(string str)
        {
            string newString = str;
            if (str.Length < 3)
            {
                newString = str.ToUpper();
            }
            else
            {
                newString = str.Substring(0, str.Length - 3) + str.Substring(str.Length - 3).ToUpper();
            }
            return newString;
        }

        //EveryNth
        public string EveryNth(string str, int n)
        {
            string newWord = "";
            if (str.Length < 1)
                return str;
            for (int i = 0; i < str.Length; i+=n)
            {
                newWord += str[i];
            }
            return newWord;
        }


    }
}
