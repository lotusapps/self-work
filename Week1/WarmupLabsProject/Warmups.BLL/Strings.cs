﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Warmups.BLL
{
    public class Strings
    {
        // Given a string name, e.g. "Bob", return a greeting of the form "Hello Bob!". 
        public string SayHi(string name)
        {
            return string.Format("Hello {0}!", name);
        }

        // Abba
        public string Abba(string a, string b)
        {
            return string.Format("{0}{1}{1}{0}", a, b);
        }

        //MakeTags
        public string MakeTags(string tag, string content)
        {
            return string.Format("<{0}>{1}<{0}>", tag, content);
        }

        //InsertWord
        public string InsertWord(string container, string word)
        {
            string first = container.Substring(0, 2);
            string second = container.Substring(2, 2);

            return string.Format("{0}{1}{2}", first, word, second);
        }

        //MultipleEndings
        public string MultipleEndings(string str)
        {
            string last = str.Substring(str.Length - 2, 2);
            return string.Format("{0}{0}{0}", last);
        }

        //FirstHalf
        public string FirstHalf(string str)
        {
            string firstHalf = str.Substring(0, str.Length/2);
            return string.Format("{0}", firstHalf);
        }

        //TrimOne
        public string TrimOne(string str)
        {
            string word = str.Substring(1, str.Length - 2);
            return string.Format("{0}", word);
        }

        //LongInMiddle
        public string LongInMiddle(string a, string b)
        {
            if (a.Length > b.Length)
            {
                return string.Format("{0}{1}{0}", b, a);
            }
            else
            {
                return string.Format("{0}{1}{0}", a, b);
            }
        }

        //RotateLeft2
        public string RotateLeft2(string str)
        {
            if (str.Length == 2)
            {
                return str;
            }
            else
            {
                string first = str.Substring(0, 2);
                string last = str.Substring(2, str.Length - 2);

                return string.Format("{0}{1}", last, first);
            }
        }

        //RotatRight2
        public string RotateRight2(string str)
        {
            if (str.Length == 2)
            {
                return str;
            }
            else
            {
                string first = str.Substring(0, str.Length - 2);
                string last = str.Substring(str.Length - 2, 2);

                return string.Format("{0}{1}", last, first);
            }
        }

        //TakeOne
        public string TakeOne(string str, bool fromFront)
        {
            if (fromFront)
            {
                string letter = str[0].ToString();
                return letter;
            }
            else
            {
                string letter = str[str.Length - 1].ToString();
                return letter;
            }
        }

        //MiddleTwo
        public string MiddleTwo(string str)
        {
            if (str.Length%2 == 0)
            {
                string middletwoletters = str.Substring((str.Length/2) - 1, 2);
                return middletwoletters;
            }
            return str;
        }

        //EndsWithLy
        public bool EndsWithLy(string str)
        {
            if (str.Length >= 2)
            {
                 if (str.Substring(str.Length - 2, 2).Contains("ly"))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
         }

        //FontAndBack
        public string FrontAndBack(string str, int n)
        {
            string front = str.Substring(0, n);
            string back = str.Substring(str.Length - n, n);

            return front + back;
        }


        //TakeTwoFromPosition
        public string TakeTwoFromPosition(string str, int n)
        {
            if (str.Length - 1 == n)
            {
                string front = str.Substring(0, 2);
                return front;
            }
        else
            {
                string text = str.Substring(n, 2);
                return text;
            } 
        }

        //HasBad
        public bool HasBad(string str)
        {
            if (str.Length < 3)
            {
                return false;
            }
            else if (str.Substring(0, 3).Contains("bad") || str.Substring(1, 3).Contains("bad"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //AtFirst
        public string AtFirst(string str)
        {
            
            if (str.Length >= 2)
            {
                char first = str[0];
                char second = str[1];

                return string.Format("{0}{1}", first, second);
            }
            else if (str.Length == 0)
            {
                return "@@";
            }
            else
            {
                char first = str[0];
                char second = '@';

                return string.Format("{0}{1}", first, second);
            }
        }

        //LastChars
        public string LastChars(string a, string b)
        {
            if (b.Length == 0 && a.Length == 0)
            {
                a = '@'.ToString();
                b = '@'.ToString();
            }
            else if (b.Length == 0)
            {
                b = '@'.ToString();
            }
            else if (a.Length == 0)
            {
                a = '@'.ToString();    
            }

            return a[0].ToString() + b[b.Length -1].ToString();
        }

        //Concat
        public string ConCat(string a, string b)
        {
            string word;

            if (a.Length == 0 || b.Length == 0)
            {
                word = string.Concat(a, b);
            }
            else if (a.Substring(a.Length - 1) == b.Substring(0, 1))
            {
                word = string.Concat(a, b.Substring(1));
            }
            else
            {
                word = string.Concat(a, b);
            }
            return word;
        }

        //SwapLast
        public string SwapLast(string str)
        {
            string word;

            if (str.Length >= 2)
            {
                char a = str[str.Length - 1];
                char b = str[str.Length - 2];

                word = str.Substring(0, str.Length - 2) + a + b;
            }
            else
            {
                word = str;
            }
            return word;
        }

        //FrontAgain
        public bool FrontAgain(string str)
        {
            if (str.Length >= 2)
            {
                string front = str.Substring(0, 2);
                string back = str.Substring(str.Length - 2, 2);

                if (front == back)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }

        //MinCat
        public string MinCat(string a, string b)
        {
            int lengthA = a.Length;
            int lengthB = b.Length;
            string textA = a;
            string textB = b;
   
                if (lengthA > lengthB)
                {
                    textA = a.Substring(lengthA - lengthB, lengthB);
                }
                else if (lengthB > lengthA)
                {
                    textB = b.Substring(lengthB - lengthA, lengthA);
                }
            return string.Concat(textA, textB);
        }

        //TweakFront
        public string TweakFront(string str)
        {
            if (str[0] != 'a' && str[1] != 'b')
            {
                return str.Substring(2);
            }
            else if (str[0] != 'a')
            {
                return str.Substring(1);
            }
            else if (str[1] != 'b')
            {
                return str[0] + str.Substring(2);
            }
            return str;
        }

        //StripX
        public string StripX(string str)
        {
            string word = str;
            if (str[0] == 'x' && str[str.Length - 1] == 'x')
            {
                word = str.Substring(1, str.Length - 2);
            }
            else if (str[0] == 'x')
            {
                word = str.Substring(1);
            }
            else if (str[str.Length - 1] == 'x')
            {
                word = str.Substring(0, str.Length - 1);
            }
            return word;
        }
    }

}


