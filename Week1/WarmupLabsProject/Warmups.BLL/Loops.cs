﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Loops
    {
        /* Given a string and a non-negative int n, return a 
           larger string that is n copies of the original string. 
        */
        public string StringTimes(string str, int n)
        {
            string result = "";

            for (int i = 1; i <= n; i++)
            {
                result += str;
            }

            return result;
        }

        public string FrontTimes(string str, int n)
        {
            string newWord = "";
            if (n < 0)
                return str;
            for (int i = 0; i < n; i++)
            {
                newWord += str.Substring(0, 3);
            }
            return newWord;
        }

        //Countxx
        public int CountXX(string str)
        {
            int counter = 0;
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (str.Substring(i, 2).Contains("xx"))
                    counter++;
            }
            return counter;
        }

        //DoubleX
        public bool DoubleX(string str)
        {
            char compare = 'x';
            bool test = false;
            int counter = 0;
            for (int i = 0; i < str.Length - 1; i++)
            {
                if (str[i] == compare)
                    counter++;
                while (counter == 1)
                {
                    if (str[i] == compare && str[i + 1] == compare)
                    {
                        test = true;
                    }
                    break;
                }
            }
            return test;
        }

        //Every Other
        public string EveryOther(string str)
        {
            string newString = "";

            for (int i = 0; i < str.Length; i += 2)
            {
                newString += str[i];
            }

            return newString;
        }

        //StringSplosion
        public string StringSplosion(string str)
        {
            string newString = "";
            for (int i = 1; i <= str.Length; i++)
            {
                newString += str.Substring(0, i);
            }
            return newString;
        }

        //CountLast2
        public int CountLast2(string str)
        {
            int counter = 0;
            string word = str.Substring(str.Length - 2, 2);
            string newString = str.Substring(0, str.Length - 2);
            for (int i = 0; i < newString.Length - 1; i++)
            {
                if (newString.Substring(i, 2).Contains(word))
                {
                    counter++;
                }
          
            }
            return counter;

        }

        //Count9
        public int Count9(int[] num)
        {
            int counter = 0;

            foreach (int n in num)
            {
                if (n == 9)
                    counter++;
            }
            return counter;
        }

        //ArrayFront9
        public bool ArrayFront9(int[] num)
        {
            bool haveNine = false;
            for (int i = 0; i < num.Length; i++)
            {
                if (num.Length < 4)
                {
                    if (num[i] == 9)
                    {
                        haveNine = true;
                    }
                }
                else
                {
                    for (int j = 0; j < 4; j++)
                    {
                        if (num[j] == 9)
                        {
                            haveNine = true;
                        }
                    }
                }      
            }
            return haveNine;
        }

        //array123
        public bool Array123(int[] num)
        {
            if (num.Contains(1) && num.Contains(2) && num.Contains(3))
                return true;
            return false;
        }

        //SubStringMatch
        public int SubStringMatch(string a, string b)
        {
            int counter = 0;
            if (a.Length < b.Length)
            {
                for (int i = 0; i < a.Length - 1; i++)
                {
                    if (a.Substring(i, 2) == b.Substring(i, 2))
                        counter++;
                }
            }
            else
            {
                for (int i = 0; i < b.Length - 1; i++)
                {
                    if (a.Substring(i, 2) == b.Substring(i, 2))
                        counter++;
                }
            }
            return counter;
        }

        //StringX
        public string StringX(string str)
        {
            string newString = "";

            if (str[0] == 'x' || str[str.Length - 1] == 'x')
            {
                newString = str.Replace("x", "");
                newString = string.Format("x" + newString + "x");
            }
            else
            {
                newString = str.Replace("x", "");
            }

            return newString;
        }

        public string AltPairs(string str)
        {
            string newWord = "";
            int[] array = {0, 1, 4, 5, 8, 9};
            foreach (int a in array)
            {
                    if(a < str.Length)
                         newWord += str[a];
            }
            return newWord;
        }

        public string DoNotYak(string str)
        {
            string newString = str;
            for (int i = 1; i < str.Length - 1; i++)
            {
                if (str[i - 1] == 'y' && str[i + 1] == 'k')
                {
                    newString = str.Remove(i - 1, 3);
                }                
            }
            return newString;
        }

        public int Array667(int[] numbers)
        {
            int counter = 0;
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if (numbers[i] == 6 && numbers[i + 1] == 6 || numbers[i + 1] == 7)
                    counter++;
                
            }
            return counter;
        }

        //NoTriples
        public bool NoTriples(int[] number)
        {
            
            for (int i = 0; i < number.Length - 1; i++)
            {
                if (number[i] == number[i + 1] && number[i + 1] == number[i + 2])
                {
                    return false;
                }
               
            }
            return true;
        }

        //Pattern51
        public bool Pattern51(int[] numbers)
        {
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if (numbers[i] == 2 && numbers[i + 1] == 7 && numbers[i + 2] == 1)
                    return true;
            }
            return false;
        }



    }//End of Class
}//Namespace
