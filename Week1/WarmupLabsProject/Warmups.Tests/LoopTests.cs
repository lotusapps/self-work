﻿using System.Security.Cryptography;
using NUnit.Framework;
using Warmups.BLL;

namespace Warmups.Tests
{
    [TestFixture]
    public class LoopTests
    {
        // arrange
        Loops obj = new Loops();

        [TestCase("Hi", 2, "HiHi")]
        [TestCase("Hi", 3, "HiHiHi")]
        [TestCase("Hi", 1, "Hi")]
        public void StringTimesTest(string str, int n, string expected)
        {
          
            // act
            string actual = obj.StringTimes(str, n);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Chocolate", 2, "ChoCho")]
        [TestCase("Chocolate", 3, "ChoChoCho")]
        [TestCase("Abc", 3, "AbcAbcAbc")]
        public void FrontTimesTest(string str, int n, string expected)
        {
            //actual
            string actual = obj.FrontTimes(str, n);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("abcxx", 1)]
        [TestCase("xxx", 2)]
        [TestCase("xxxx", 3)]
        public void CountXXTest(string str, int expected)
        {
            //actual
            int actual = obj.CountXX(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("axxbb", true)]
        [TestCase("axaxxax", false)]
        [TestCase("xxxxx", true)]
        public void DoubleXTest(string str, bool expected)
        {
            //actual
            bool actual = obj.DoubleX(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "Hlo")]
        [TestCase("Hi", "H")]
        [TestCase("Heeololeo", "Hello")]
        public void EveryOtherTest(string str, string expected)
        {
            //actual
            string actual = obj.EveryOther(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Code", "CCoCodCode")]
        [TestCase("abc", "aababc")]
        [TestCase("ab", "aab")]
        public void StringSplosionTest(string str, string expected)
        {
            //actual
            string actual = obj.StringSplosion(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("hixxhi", 1)]
        [TestCase("xaxxaxaxx", 1)]
        [TestCase("axxxaaxx", 2)]
        public void CountLast2Test(string str, int expected)
        {
            //actual
            int actual = obj.CountLast2(str);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(new int[] {1, 2, 9}, 1)]
        [TestCase(new int[] {1, 9, 9}, 2)]
        [TestCase(new int[] {1, 9, 9, 3, 9}, 3)]
        public void Count9Test(int[] num, int expected)
        {
            //actual
            int actual = obj.Count9(num);

            //expected
            Assert.AreEqual(expected, actual);
        }

        [TestCase(new int[] {1, 2, 9, 3, 4}, true)]
        [TestCase(new int[] {1, 2, 3, 4, 9}, false)]
        [TestCase(new int[] {1, 2, 3, 4, 5}, false)]
        public void ArrayFront9Test(int[] num, bool expected)
        {
            //actuall
            bool actual = obj.ArrayFront9(num);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(new int[] {1, 1, 2, 3, 1}, true)]
        [TestCase(new int[] {1, 1, 2, 4, 1}, false)]
        [TestCase(new int[] {1, 1, 2, 1, 2, 3}, true)]
        public void Array123Test(int[] num, bool expected)
        {
            //actual
            bool actual = obj.Array123(num);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("xxcaazz", "xxbaaz", 3)]
        [TestCase("abc", "abc", 2)]
        [TestCase("abc", "axc", 0)]
        public void SubStringMatchTest(string a, string b, int expected)
        {
            //actual
            int actual = obj.SubStringMatch(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("xxHxix", "xHix")]
        [TestCase("abxxxcd", "abcd")]
        [TestCase("xabxxxcdx", "xabcdx")]
        public void StringX(string str, string expected)
        {
            //actual
            string actual = obj.StringX(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("kitten", "kien")]
        [TestCase("Chocolate", "Chole")]
        [TestCase("CodingHorror", "Congrr")]
        public void AltPairs(string str, string expected)
        {
            //actual
            string actual = obj.AltPairs(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("yakpak", "pak")]
        [TestCase("pakyak", "pak")]
        [TestCase("yak123ya", "123ya")]
        public void DoNotYakTest(string str, string expected)
        {
            //actual
            string actual = obj.DoNotYak(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(new int[] {6, 6, 2}, 1)]
        [TestCase(new int[] {6, 6, 2, 6}, 1)]
        [TestCase(new int[] {6, 7, 2, 6}, 1)]
        public void Array667Test(int[] numbers, int expected)
        {
            //actual
            int actual = obj.Array667(numbers);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(new int[] {1, 1, 2, 2, 1}, true)]
        [TestCase(new int[] {1, 1, 2, 2, 2, 1}, false)]
        [TestCase(new int[] {1, 1, 1, 2, 2, 2, 1}, false)]
        public void NoTriples(int[] numbers, bool expected)
        {
            //actual
            bool actual = obj.NoTriples(numbers);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(new int[] {1, 2, 7, 1}, true)]
        [TestCase(new int[] {1, 2, 8, 1}, false)]
        [TestCase(new int[] {2, 7, 1}, true)]
        public void Pattern51Test(int[] numbers, bool expected)
        {
            //actual
            bool actual = obj.Pattern51(numbers);

            //assert
            Assert.AreEqual(expected, actual);
        }


    }
}
