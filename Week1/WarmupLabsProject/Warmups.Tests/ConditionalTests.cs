﻿using System.ComponentModel;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using Warmups.BLL;

namespace Warmups.Tests
{
    [TestFixture]
    public class ConditionalTests
    {
        Conditionals obj = new Conditionals();

        [TestCase(true, true, true)]
        [TestCase(false, false, true)]
        [TestCase(true, false, false)]
        public void AreWeInTroubleTest(bool aSmile, bool bSmile, bool expected)
        {
            // act
            bool actual = obj.AreWeInTrouble(aSmile, bSmile);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(false, false, true)]
        [TestCase(true, false, false)]
        [TestCase(false, true, true)]
        public void CanSleepInTest(bool isWeekday, bool isVacaton, bool expected)
        {
            //actual
            bool actual = obj.CanSleepIn(isWeekday, isVacaton);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(1, 2, 3)]
        [TestCase(3, 2, 5)]
        [TestCase(2, 2, 8)]
        public void SumDoubleTest(int a, int b, int expected)
        {
            //actual
            int actual = obj.SumDouble(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(23, 4)]
        [TestCase(10, 11)]
        [TestCase(21, 0)]
        public void Diff21(int n, int expected)
        {
            //actual
            int actual = obj.Diff21(n);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(true, 6, true)]
        [TestCase(true, 7, false)]
        [TestCase(false, 6, false)]
        public void ParrotTroubleTest(bool isTalking, int hour, bool expected)
        {
            //actual
            bool actual = obj.ParrotTrouble(isTalking, hour);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(9, 10, true)]
        [TestCase(9, 9, false)]
        [TestCase(1, 9, true)]
        public void Makes10Test(int a, int b, bool expected)
        {
            //actual
            bool actual = obj.Makes10(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(103, true)]
        [TestCase(90, true)]
        [TestCase(89, false)]
        [TestCase(195, true)]
        [TestCase(206, true)]
        public void NearHundredTest(int n, bool expected)
        {
            //actual 
            bool actual = obj.NearHundred(n);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(1, -1, false, true)]
        [TestCase(-1, 1, false, true)]
        [TestCase(-4, -5, true, true)]
        public void PosNegTest(int a, int b, bool negative, bool expected)
        {
            //actual
            bool actual = obj.PosNeg(a, b, negative);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("candy", "not candy")]
        [TestCase("x", "not x")]
        [TestCase("not bad", "not bad")]
        public void NotString(string s, string expected)
        {
            //actual
            string actual = obj.NotString(s);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("kitten", 1, "ktten")]
        [TestCase("kitten", 0, "itten")]
        [TestCase("kitten", 4, "kittn")]
        public void MissingCharTest(string str, int n, string expected)
        {
            //actual
            string actual = obj.MissingChar(str, n);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("code", "eodc")]
        [TestCase("a", "a")]
        [TestCase("ab", "ba")]
        [TestCase("emmy", "ymme")]
        public void FrontBackTest(string str, string expected)
        {
            //actual
            string actual = obj.FrontBack(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Microsoft", "MicMicMic")]
        [TestCase("Chocolate", "ChoChoCho")]
        [TestCase("at", "atatat")]
        public void Front3(string str, string expected)
        {
            //actual
            string actual = obj.Front3(str);

            //assert    
            Assert.AreEqual(expected, actual);
        }

        [TestCase("cat", "tcatt")]
        [TestCase("Hello", "oHelloo")]
        [TestCase("a", "aaa")]
        public void BackAroundTest(string str, string expected)
        {
            //actual
            string actual = obj.BackAround(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(3, true)]
        [TestCase(10, true)]
        [TestCase(8, false)]
        public void Multiple3Or5Test(int number, bool expected)
        {
            //actual
            bool actual = obj.Multiple3Or5(number);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("hi there", true)]
        [TestCase("hi", true)]
        [TestCase("high up", false)]
        public void StartHiTest(string str, bool expected)
        {
            //actual
            bool actual = obj.StartHi(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(120, -1, true)]
        [TestCase(120, -1, true)]
        [TestCase(120, -1, true)]
        public void IcyHotTest(int temp1, int temp2, bool expected)
        {
            //actual
            bool actual = obj.IcyHot(temp1, temp2);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(12, 99, true)]
        [TestCase(21, 12, true)]
        [TestCase(8, 99, false)]
        public void Between10And20Test(int a, int b, bool expected)
        {
            //actual
            bool actual = obj.Between10And20(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(13, 20, 10, true)]
        [TestCase(20, 19, 10, true)]
        [TestCase(20, 20, 12, false)]
        public void HasTeenTest(int a, int b, int c, bool expected)
        {
            //actual
            bool actual = obj.HasTeen(a, b, c);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(13, 99, true)]
        [TestCase(21, 19, true)]
        [TestCase(13, 13, false)]
        public void SoAloneTest(int a, int b, bool expected)
        {
            //actual
            bool actual = obj.SoAlone(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("adelbc", "abc")]
        [TestCase("adelHello", "aHello")]
        [TestCase("adedbc", "adedbc")]
        public void RemoveDelTest(string str, string expected)
        {
            //actual
            string actual = obj.RemoveDel(str);

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestCase("mix snacks", true)]
        [TestCase("pix snacks", true)]
        [TestCase("piz snacks", false)]
        public void IxStartTest(string str, bool expected)
        {
            //actual
            bool actual = obj.IxStart(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("ozymandias", "oz")]
        [TestCase("bzoo", "z")]
        [TestCase("oxx", "o")]
        [TestCase("","")]
        public void StartOzTest(string str, string expected)
        {
            //actual
            string actual = obj.StartOz(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(1, 2, 3, 3)]
        [TestCase(1, 3, 2, 3)]
        [TestCase(3, 2, 1, 3)]
        [TestCase(1, 8, 24, 24)]
        public void MaxTest(int a, int b, int c, int expected)
        {
            //actual
            int actual = obj.Max(a, b, c);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(8, 13, 8)]
        [TestCase(13, 8, 8)]
        [TestCase(13, 7, 0)]
        public void CloserTest(int a, int b, int expected)
        {
            //actual
            int actual = obj.Closer(a, b);

            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", true)]
        [TestCase("Heelle", true)]
        [TestCase("Heelele", false)]
        public void GotETest(string str, bool expected)
        {
            //actual
            bool actual = obj.GotE(str);
            
            //Assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "HeLLO")]
        [TestCase("hi there", "hi thERE")]
        [TestCase("hi", "HI")]
        public void EndUpTest(string str, string expected)
        {
            //actual
            string actual = obj.EndUp(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Miracle", 2, "Mrce")]
        [TestCase("abcdefg", 2, "aceg")]
        [TestCase("abcdefg", 3, "adg")]
        public void EveryNthTest(string str, int n, string expected)
        {
            //actual
            string actual = obj.EveryNth(str, n);

            //assert
            Assert.AreEqual(expected, actual);
        }

    }
}
