﻿using System;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using Warmups.BLL;

namespace Warmups.Tests
{
    [TestFixture]
    public class StringTests
    {
        private Strings obj = new Strings();

        [TestCase("Bob", "Hello Bob!")]
        [TestCase("Alice", "Hello Alice!")]
        [TestCase("X", "Hello X!")]
        public void SayHiTest(string name, string expected)
        {
            // act
            string actual = obj.SayHi(name);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hi", "Bye", "HiByeByeHi")]
        [TestCase("Yo", "Alice", "YoAliceAliceYo")]
        [TestCase("What", "Up", "WhatUpUpWhat")]
        public void AbbaTest(string a, string b, string expected)
        {
            //act 
            string actual = obj.Abba(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("i", "Yay", "<i>Yay<i>")]
        [TestCase("i", "Hello", "<i>Hello<i>")]
        [TestCase("cite", "Yay", "<cite>Yay<cite>")]
        public void MakeTagsTest(string tag, string content, string expected)
        {

            //act
            string actual = obj.MakeTags(tag, content);


            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("<<>>", "Yay", "<<Yay>>")]
        [TestCase("<<>>", "WooHoo", "<<WooHoo>>")]
        [TestCase("[[]]", "word", "[[word]]")]
        public void InsertWordTest(string container, string word, string expected)
        {
            //actual
            string actual = obj.InsertWord(container, word);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "lololo")]
        [TestCase("ab", "ababab")]
        [TestCase("Hi", "HiHiHi")]
        public void MultipleEndingsTest(string str, string expected)
        {
            //actual
            string actual = obj.MultipleEndings(str);

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestCase("WooHoo", "Woo")]
        [TestCase("HelloThere", "Hello")]
        [TestCase("abcdef", "abc")]
        public void firstHalfTest(string str, string expected)
        {
            //actual
            string actual = obj.FirstHalf(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "ell")]
        [TestCase("Java", "av")]
        [TestCase("coding", "odin")]
        public void TrimOneTest(string str, string expected)
        {
            //actual
            string actual = obj.TrimOne(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "hi", "hiHellohi")]
        [TestCase("hi", "Hello", "hiHellohi")]
        [TestCase("aaa", "bb", "bbaaabb")]
        public void LongInMiddleTest(string a, string b, string expected)
        {
            //actual
            string actual = obj.LongInMiddle(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "lloHe")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void RotateLeft2Test(string str, string expected)
        {
            //actual
            string actual = obj.RotateLeft2(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "loHel")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void RotateRight2Test(string str, string expected)
        {
            //actual
            string actual = obj.RotateRight2(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", true, "H")]
        [TestCase("Hello", false, "o")]
        [TestCase("oh", true, "o")]
        public void TakeOneTest(string str, bool fromFront, string expected)
        {
            //actual
            string actual = obj.TakeOne(str, fromFront);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("string", "ri")]
        [TestCase("code", "od")]
        [TestCase("Practice", "ct")]
        public void MiddleTwoTest(string str, string expected)
        {
            //actual
            string actual = obj.MiddleTwo(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("oddly", true)]
        [TestCase("y", false)]
        [TestCase("oddy", false)]
        public void EndsWithLyTest(string str, bool expected)
        {
            //actual
            bool actual = obj.EndsWithLy(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", 2, "Helo")]
        [TestCase("Chocolate", 3, "Choate")]
        [TestCase("Chocolate", 1, "Ce")]
        public void FrontAndBackTest(string str, int n, string expected)
        {
            //actual
            string actual = obj.FrontAndBack(str, n);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("java", 0, "ja")]
        [TestCase("java", 2, "va")]
        [TestCase("java", 3, "ja")]
        [TestCase("Monkey", 4, "ey")]
        public void TakeTwoFromPositionTest(string str, int n, string expected)
        {
            //actual
            string actual = obj.TakeTwoFromPosition(str, n);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("badxx", true)]
        [TestCase("xbadxx", true)]
        [TestCase("xxbadxx", false)]
        [TestCase("b", false)]
        [TestCase("", false)]
        public void HasBadTest(string str, bool expected)
        {
            //actual
            bool actual = obj.HasBad(str);

            //assert
            Assert.AreEqual(expected, actual);

        }

        [TestCase("hello", "he")]
        [TestCase("hi", "hi")]
        [TestCase("h", "h@")]
        [TestCase("", "@@")]
        public void AtFirstTest(string str, string expected)
        {
            //actual
            string actual = obj.AtFirst(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("last", "chars", "ls")]
        [TestCase("yo", "mama", "ya")]
        [TestCase("hi", "", "h@")]
        [TestCase("","","@@")]
        public void LastCharsTest(string a, string b, string expected)
        {
            //actual
            string actual = obj.LastChars(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("abc", "cat", "abcat")]
        [TestCase("dog", "cat", "dogcat")]
        [TestCase("abc", "", "abc")]
        public void ConCatTest(string a, string b, string expected)
        {
            //actual
            string actual = obj.ConCat(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("coding", "codign")]
        [TestCase("cat", "cta")]
        [TestCase("ab", "ba")]
        [TestCase("","")]
        public void SwapLastTest(string str, string expected)
        {
            //actual
            string actual = obj.SwapLast(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("edited", true)]
        [TestCase("edit", false)]
        [TestCase("ed", true)]
        public void FrontAgainTest(string str, bool expected)
        {
            //actual
            bool actual = obj.FrontAgain(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "Hi", "loHi")]
        [TestCase("Hello", "java", "ellojava")]
        [TestCase("java", "Hello", "javaello")]
        public void MinCat(string a, string b, string expected)
        {
            //actual
            string actual = obj.MinCat(a, b);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "llo")]
        [TestCase("away", "aay")]
        [TestCase("abed", "abed")]
        public void TweakFrontTest(string str, string expected)
        {
            //actual
            string actual = obj.TweakFront(str);

            //assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("xHix", "Hi")]
        [TestCase("xHi", "Hi")]
        [TestCase("Hxix", "Hxi")]
        public void StripX(string str, string expected)
        {
            //actual 
            string actual = obj.StripX(str);

            //assert
            Assert.AreEqual(expected, actual);
        }
    }
}
