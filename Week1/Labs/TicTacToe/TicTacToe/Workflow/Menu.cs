﻿using System;
using System.Diagnostics;
using System.Reflection.Emit;

namespace TicTacToe.Workflow
{
    class Menu
    {   
        Game game = new Game();
        
        char x1 = '1';
        char x2 = '2';
        char x3 = '3';
        char x4 = '4';
        char x5 = '5';
        char x6 = '6';
        char x7 = '7';
        char x8 = '8';
        char x9 = '9';

        private bool playAgain = true;

        public void startMenu()
        { 
            displayTitle();
            PromptPlayersName();
            Console.Clear();        
            do
            {
                Game start = new Game();
                start.StartGame();
                playAgain = start.playAgainChoice();
            } while (playAgain);
               
        }
        public ConsoleColor GetColor(char c)
        {
            ConsoleColor color;
            if (c != 'X' && c != 'O')
                color = ConsoleColor.DarkGray;
            else
            {
                color = ConsoleColor.Yellow;
            }

            return color;
        }

        public void displayMoves(Player p1, Player p2)
        {
            for (int i = 1; i <= 2; i++)
            {
                ConsoleColor playerColor;

                string name;
                if (i == 1)
                    name = p1.PlayerName;
                else
                {
                    name = p2.PlayerName;
                }
                if (playCounter % 2 == 0)
                {
                    playerColor = ConsoleColor.Magenta;
                }
                else
                {
                    playerColor = ConsoleColor.Cyan;
                }
                checkForWin(p1, p2);
                if (playCounter == 10)
                    break;
                Console.Write("It is ");
                Console.ForegroundColor = playerColor;
                Console.Write("{0}", name);
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("'s Turn, select number: ");
                getMoves();
                displayGameDuring(p1, p2);
            }
        }

        public void displayScore(Player p1, Player p2)
        {
            Console.WriteLine("");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("{0,12} -\t", p1.PlayerName);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0,2}", p1.PlayerScore);
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.Write("{0, 12} -\t", p2.PlayerName);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0,2}", p2.PlayerScore);
        }
        public void displayGameDuring(Player p1, Player p2)
        {
            ConsoleColor color;

            Console.Clear();
            displayScore(p1, p2);
            Console.WriteLine("\t\t\t     |     |     ");
            Console.ForegroundColor = GetColor(x7);
            Console.Write("\t\t\t  {0}  ", x7);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("|");
            Console.ForegroundColor = GetColor(x8);
            Console.Write("  {0}  ", x8);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("|");
            Console.ForegroundColor = GetColor(x9);
            Console.WriteLine("  {0}  ", x9);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\t\t\t     |     |     ");
            Console.WriteLine("\t\t\t-----------------");
            Console.WriteLine("\t\t\t     |     |     ");
            Console.ForegroundColor = GetColor(x4);
            Console.Write("\t\t\t  {0}  ", x4);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("|");
            Console.ForegroundColor = GetColor(x5);
            Console.Write("  {0}  ", x5);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("|");
            Console.ForegroundColor = GetColor(x6); ;
            Console.WriteLine("  {0}  ", x6);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\t\t\t     |     |     ");
            Console.WriteLine("\t\t\t-----------------");
            Console.WriteLine("\t\t\t     |     |     ");
            Console.ForegroundColor = GetColor(x1);
            Console.Write("\t\t\t  {0}  ", x1);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("|");
            Console.ForegroundColor = GetColor(x2);
            Console.Write("  {0}  ", x2);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("|");
            Console.ForegroundColor = GetColor(x3);
            Console.WriteLine("  {0}  ", x3);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\t\t\t     |     |     \n\n");
            Console.Beep();
        }
        public void PromptPlayersName()
        {
            Console.Write("Enter player 1 name: ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            string name = Console.ReadLine();
            game.GetPlayerName(name);
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write("Enter player 2 name: ");
            Console.ForegroundColor = ConsoleColor.Magenta;
            string name2 = Console.ReadLine();
            game.GetPlayerName(name);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void displayTitle()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  Tic  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            Console.WriteLine("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  Tac  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            Console.WriteLine("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  Toe  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            Console.WriteLine("-------------------------------------------------------------------------------\n");
            Console.ForegroundColor = ConsoleColor.White;
        }

    }
}
