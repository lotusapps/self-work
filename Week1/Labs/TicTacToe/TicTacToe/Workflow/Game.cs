﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using TicTacToe.Workflow;

namespace TicTacToe
{
    class Game
    {
        private readonly Menu _menu;

        public Game(Menu menu)
        {
            _menu = menu;
        }
        public Game()
        {
           
        }

        public void GetPlayerName(string name)
        {
            Player player = new Player();
            player.PlayerName = name;
        }
        public int playCounter = 1;

        char x1 = '1';
        char x2 = '2';
        char x3 = '3';
        char x4 = '4';
        char x5 = '5';
        char x6 = '6';
        char x7 = '7';
        char x8 = '8';
        char x9 = '9';
        int[] numberOfSlots = {1, 2, 3, 4, 5, 6, 7, 8, 9};

        public void StartGame()
        {
            _menu.displayGameDuring();

            do
            {
                _menu.displayMoves();

            } while (playCounter < 10);
        }   

        public void getMoves()
        {
            bool isValid = false;
            int userLocation;
            do
            {      
                Console.ForegroundColor = ConsoleColor.Yellow;
                string userInput = Console.ReadLine();
                Console.ForegroundColor = ConsoleColor.White;
                isValid = int.TryParse(userInput, out userLocation);
                if (isValid == false)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("Invalid Selection, Pick a location: ");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                else
                {
                    if (userLocation < 1 || userLocation > 9)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("Invalid Choice, Select another number: ");
                        Console.ForegroundColor = ConsoleColor.White;
                        isValid = false;
                    }
                    else if (!numberOfSlots.Contains(userLocation))
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("Can't go there, Select another number: ");
                        Console.ForegroundColor = ConsoleColor.White;
                        isValid = false;
                    }
                    else
                    {
                        numberOfSlots[userLocation - 1] = 0;
                        switch (userLocation)
                        {
                            case 1:
                                isValid = true;
                                x1 = determineXO();
                                break;
                            case 2:
                                isValid = true;
                                x2 = determineXO();
                                break;
                            case 3:
                                isValid = true;
                                x3 = determineXO();
                                break;
                            case 4:
                                isValid = true;
                                x4 = determineXO();
                                break;
                            case 5:
                                isValid = true;
                                x5 = determineXO();
                                break;
                            case 6:
                                isValid = true;
                                x6 = determineXO();
                                break;
                            case 7:
                                isValid = true;
                                x7 = determineXO();
                                break;
                            case 8:
                                isValid = true;
                                x8 = determineXO();
                                break;
                            case 9:
                                isValid = true;
                                x9 = determineXO();
                                break;
                            default:
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Write("Invalid Choice, Select another number.");
                                Console.ForegroundColor = ConsoleColor.White;
                                break;
                        }
                        
                    }          
                }
            } while (isValid == false);          
        }

        public char determineXO()
        {
            playCounter++;
            if (playCounter%2 == 0)
                return 'X';
            return 'O';

        }

        public void checkForWin(Player p1, Player p2)
        {
            if ((x1 == x2 && x2 == x3) || (x4 == x5 && x5 == x6) || (x7 == x8 && x8 == x9) ||
                (x1 == x4 && x4 == x7) || (x2 == x5 && x5 == x8) || (x3 == x6 && x6 == x9) ||
                (x1 == x5 && x5 == x9) || (x7 == x5 && x5 == x3))
                
            {
                if (playCounter%2 == 0)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("{0, 12} is the Winner!\n", p1.PlayerName.ToUpper());
                    Console.Beep();
                    Console.Beep();
                    Console.ForegroundColor = ConsoleColor.White;
                    p1.PlayerScore += 1;
                    playCounter = 10;
                    
                }      
                else
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("{0, 12} is the Winner!\n", p2.PlayerName.ToUpper());
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Beep();
                    Console.Beep();
                    p2.PlayerScore += 1;
                    playCounter = 10;
                }
            }
            else if(playCounter == 10)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\tDRAW!\n");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        public bool playAgainChoice()
        {
            string answer;
            string upperAnswer;
            do
            {
                Console.Write("Would you like to play again? Y or N : ");
                answer = Console.ReadLine();
                upperAnswer = answer.ToUpper();
                if (upperAnswer != "Y" && upperAnswer != "N")
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("That is not an answer, it's a simple yes or no question.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
            } while (upperAnswer != "Y" && upperAnswer != "N");

            if (upperAnswer == "N")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("Goodbye");
                Thread.Sleep(400);
                Console.Write(".");
                Thread.Sleep(400);
                Console.Write(".");
                Thread.Sleep(400);
                Console.Write(".");
                Console.ForegroundColor = ConsoleColor.White;
                return false;
            }
            else
            {
                return true;
            }
        }

        public ConsoleColor GetColor(char c)
        {
            ConsoleColor color;
            if(c != 'X' && c!= 'O')
                color = ConsoleColor.DarkGray;
             else
            {
                    color = ConsoleColor.Yellow;      
            }

            return color;
        }

        /// <summary>
        /// hold players information
        /// </summary>
        private Player[] Players;
    }
}
