﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Workflow;

namespace TicTacToe
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu menu = new Menu();
            Game game = new Game(menu);
            menu.startMenu();
        }
    }
}
