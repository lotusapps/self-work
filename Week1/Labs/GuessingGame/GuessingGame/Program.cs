﻿using System;

namespace GuessingGame
{
    class Program
    {
        static void Main(string[] args)
        {
            string playerName;
            string playerInput;
            string level;
            string range;
            bool levelSelected = false;
            int playerGuess;
            int answer = 0;
            int high = 0;
            int playCounter = 10;
            bool inCorrect = true;

            Random r = new Random();


            Console.WriteLine("-------------- Guessing Game --------------\n\n");
            Console.Write("What is your name? : ");
            playerName = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("\nHello {0}, I'd like to play a game\nPress Q at anytime to quit.\n", playerName);

            do
            {
                Console.WriteLine("How would you like it? Choose level number.\n");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("(1)Easy");
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("(2)Medium");
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("(3)Hard");
                Console.ForegroundColor = ConsoleColor.DarkMagenta;
                Console.WriteLine("(4)Boss");
                Console.ForegroundColor = ConsoleColor.White;

                level = Console.ReadLine();

                switch (level)
                {
                    case "1":
                        levelSelected = true;
                        Console.Clear();
                        range = "Guess the number between 1 and 20";
                        Console.WriteLine("Level 1 Selected\n{0}", range);
                        answer = r.Next(1, 21);
                        high = 20;
                        break;
                    case "2":
                        levelSelected = true;
                        Console.Clear();
                        range = "Guess the number between 1 and 50";
                        Console.WriteLine("Level 2 Selected\n{0}", range);
                        answer = r.Next(1, 51);
                        high = 50;
                        break;
                    case "3":
                        levelSelected = true;
                        Console.Clear();
                        range = "Guess the number between 1 and 100";
                        Console.WriteLine("Level 3 Selected\n{0}", range);
                        answer = r.Next(1, 101);
                        high = 100;
                        break;
                    case "4":
                        levelSelected = true;
                        Console.Clear();
                        range = "Guess the number between 1 and 1000";
                        Console.WriteLine("Level 4 Selected\n{0}", range);
                        answer = r.Next(1, 1001);
                        high = 1000;
                        break;
                    default:
                        Console.Clear();
                        Console.ForegroundColor = ConsoleColor.DarkRed;
                        Console.WriteLine("Incorrect level selected!\n");
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                }

            } while (!levelSelected);


            do
            {
                Console.WriteLine("\nYou have {0} tries left!", playCounter);
                playerInput = Console.ReadLine();
                if (playerInput.ToUpper() == "Q")
                    break;
                if (int.TryParse(playerInput, out playerGuess) && playerGuess > 1 && playerGuess <= high)
                {
                    playCounter--;

                    if (playerGuess == answer)
                    {
                        if (playCounter == 9)
                        {
                            Console.WriteLine("Congratulation! You found the number in 1 TRY! You're very Special");
                        }
                        
                        inCorrect = false;
                    }
                    else
                    {
                        if (playerGuess < answer)
                        {
                            Console.WriteLine("Higher");
                            if (Math.Abs(playerGuess - answer) <= 5)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkGreen;
                                Console.WriteLine("Very Close");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                        else if(playerGuess > answer)
                        {
                            Console.WriteLine("Lower");
                            if (Math.Abs(playerGuess - answer) <= 5)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkGreen;
                                Console.WriteLine("Very Close");
                                Console.ForegroundColor = ConsoleColor.White;
                            }
                        }
                    }
                }
                else if(playerGuess < 1 || playerGuess > high)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("That is not between 1 and {0}\n", high);
                    Console.ForegroundColor = ConsoleColor.White;
                }
                //else
                //{
                //    Console.ForegroundColor = ConsoleColor.DarkRed;
                //    Console.WriteLine("Invalid Number\n");
                //    Console.ForegroundColor = ConsoleColor.White;
                //}
                if (playCounter == 0)
                    break;

            } while (inCorrect);


            if(playCounter == 0)
            {
                Console.WriteLine("\nToo Bad, Out of tries!");
            }
            else if (playCounter != 0)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Congratulation! You found the number! Took you {0} tries, here's some cake!\n",10 - playCounter);
                Console.ForegroundColor = ConsoleColor.White;
            }
            else if (inCorrect)
            {
                Console.WriteLine("You Quitter!!!");
            }

            Console.ReadKey();

        }

        
    }
}


