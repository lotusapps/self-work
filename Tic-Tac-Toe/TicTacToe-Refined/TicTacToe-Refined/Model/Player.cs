﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe_Refined.Model
{
    class Player
    {
        public int PlayerID { set; get; }
        public string PlayerName { set; get; }
        public int Wins { set; get; }
    }

}
