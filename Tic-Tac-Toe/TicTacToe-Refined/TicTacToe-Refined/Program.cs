﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe_Refined.BLL;
using TicTacToe_Refined.View;

namespace TicTacToe_Refined
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu menu = new Menu();
            Game game = new Game(menu);
            game.Start();
            //game.SetNewGame();

        }
    }
}
