﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe_Refined.Model;

namespace TicTacToe_Refined.Data
{
    class PlayerList
    {
        List<Player> ListOfPlayers = new List<Player>();

        public List<Player> ReturnList()
        {
            return ListOfPlayers;
        }
         
        public void AddPlayerToList(Player p)
        {
            ListOfPlayers.Add(p);
        }

        public string GetPlayerName(Player p)
        {
            return p.PlayerName;
        }
    }
}
