﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Services;
using System.Text;
using System.Threading.Tasks;
using TicTacToe_Refined.BLL;
using TicTacToe_Refined.Model;

namespace TicTacToe_Refined.View
{
    class Menu
    {
        public void DisplayTitle()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("-------------------------------------------------------------------------------");
            Console.WriteLine("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  Tic  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            Console.WriteLine("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  Tac  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            Console.WriteLine("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  Toe  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            Console.WriteLine("-------------------------------------------------------------------------------\n");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void DisplayGameBoard(ConsoleColor[] color, char[] moves)
        {
            //ConsoleColor color;
            Console.Clear();
            Console.WriteLine("\t\t\t     |     |     ");
            Console.ForegroundColor = color[6];
            Console.Write("\t\t\t  {0}  ", moves[6]);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("|");
            Console.ForegroundColor = color[7];
            Console.Write("  {0}  ", moves[7]);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("|");
            Console.ForegroundColor = color[8];
            Console.WriteLine("  {0}  ", moves[8]);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\t\t\t     |     |     ");
            Console.WriteLine("\t\t\t-----------------");
            Console.WriteLine("\t\t\t     |     |     ");
            Console.ForegroundColor = color[3];
            Console.Write("\t\t\t  {0}  ", moves[3]);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("|");
            Console.ForegroundColor = color[4];
            Console.Write("  {0}  ", moves[4]);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("|");
            Console.ForegroundColor = color[5]; ;
            Console.WriteLine("  {0}  ", moves[5]);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\t\t\t     |     |     ");
            Console.WriteLine("\t\t\t-----------------");
            Console.WriteLine("\t\t\t     |     |     ");
            Console.ForegroundColor = color[0];
            Console.Write("\t\t\t  {0}  ", moves[0]);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("|");
            Console.ForegroundColor = color[1];
            Console.Write("  {0}  ", moves[1]);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("|");
            Console.ForegroundColor = color[2];
            Console.WriteLine("  {0}  ", moves[2]);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("\t\t\t     |     |     \n\n");
            Console.Beep();
        }

        public string PromptPlayerName(int playerNumber)
        {
            Console.Write("Enter Player {0}'s Name: ", playerNumber);
            Console.ForegroundColor = ConsoleColor.Cyan;
            string name = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.White;

            return name;
        }

        public string PromptUserInput()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            string userInput = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.White;
            
            return userInput;
        }

        public void Prompt(string name)
        {
            Console.Write("It is ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("{0}", name);
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("'s turn, select a position: ");
        }

        public void DisplayName(List<Player> players)
        {
            foreach (Player n in players)
            {
                Console.WriteLine("Player = {0}", n.PlayerName);
            }
        }

        public void NotANumberInputMessage()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Invalid Selection, Pick a location: ");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void OutOfRangeMessage()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Invalid Choice, Select another number: ");
            Console.ForegroundColor = ConsoleColor.White;      
        }

        public void UnavailableLocationMessage()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Can't go there, Select another number: ");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void WinningMessage(string name)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("{0, 12} is the Winner!\n", name.ToUpper());
            Console.Beep();
            Console.Beep();
            Console.ForegroundColor = ConsoleColor.White;
           
        }

        public void DrawGameMessage()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("\tDRAW!\n");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public string PromptNewGameMessage()
        {
            string input;
            Console.Write("Would you like to play again? Y or N: ");
            input = Console.ReadLine();
            return input;
        }

        public void EndGameMessage()
        {
            Console.WriteLine("GOODBYE!");
            Console.ReadKey();
        }
    }
}
