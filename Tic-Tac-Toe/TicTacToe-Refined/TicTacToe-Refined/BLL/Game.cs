﻿using System;
using System.Collections.Generic;
using TicTacToe_Refined.Model;
using TicTacToe_Refined.View;

namespace TicTacToe_Refined.BLL
{
    class Game
    {
        private readonly Menu _menu ;
        private List<Player> players = new List<Player>();
        public char[] tableOptions = new char[]{ '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        public int PlayerCounter = 1;
        public Game(Menu menu)
        {
            _menu = menu;
        }

        public void Start()
        {
            _menu.DisplayTitle();
            GetPlayerName();
            do
            {
                _menu.DisplayGameBoard(GetColor(tableOptions), tableOptions);
                _menu.Prompt(CheckPlayersTurnName());
                GetUserMoveInput();
                checkForWin();
            } while (PlayerCounter < 10);
           SetNewGame();
            Console.Read();
        }

        public void GetPlayerName()
        {
            for (int i = 1; i <= 2; i++)
            {
                Player p = new Player();
                p.PlayerName =_menu.PromptPlayerName(i);
                p.PlayerID = i;               
                players.Add(p);
            }
        }

        public void GetUserMoveInput()
        {
            bool isValid = false;
            int userLocation;
            do
            {             
                isValid = int.TryParse(_menu.PromptUserInput(), out userLocation);
                if (isValid == false)
                {
                    _menu.NotANumberInputMessage();
                }
                else
                {
                    if (userLocation < 1 || userLocation > 9)
                    {
                        _menu.OutOfRangeMessage();
                        isValid = false;
                    }
                    else if (IsAvailable(userLocation) == false)
                    {
                        _menu.UnavailableLocationMessage();
                        isValid = false;
                    }
                    else
                    {
                        PlayerCounter++;
                        tableOptions[userLocation - 1] = GetCurrentPlayerMark();
                        
                        isValid = true; 

                    }
                }
              
            } while (isValid == false);
        }

        public bool IsAvailable(int input)
        {
            bool available = int.TryParse(tableOptions[input - 1].ToString(), out input);

            
            return available;
        }

        public ConsoleColor[] GetColor(char[] characters)
        {
            ConsoleColor[] color = new ConsoleColor[9];

            for (int i = 0; i <= characters.Length - 1; i++)
            {
                if (characters[i] != 'X' && characters[i] != 'O')
                {
                    color[i] = ConsoleColor.DarkGray;
                }
                else
                {
                    color[i] = ConsoleColor.Cyan;
                }
            }
                
            return color;
        }

        public char GetCurrentPlayerMark()
        {
            char current;
            
                if (PlayerCounter%2 == 0)
                {
                    current = 'X';
                }
                else
                {
                    current = 'O';
                }

            return current;
        }

        public string CheckPlayersTurnName()
        {
            if (PlayerCounter%2 == 0)
            {
                return players[1].PlayerName;
            }
            else
            {
                return players[0].PlayerName;
            }
        }

        public void checkForWin()
        {
            if ((tableOptions[0] == tableOptions[1] && tableOptions[1] == tableOptions[2]) || 
                (tableOptions[3] == tableOptions[4] && tableOptions[4] == tableOptions[5]) ||
                (tableOptions[6] == tableOptions[7] && tableOptions[7] == tableOptions[8]) ||
                (tableOptions[0] == tableOptions[3] && tableOptions[3] == tableOptions[6]) || 
                (tableOptions[1] == tableOptions[4] && tableOptions[4] == tableOptions[7]) || 
                (tableOptions[2] == tableOptions[5] && tableOptions[5] == tableOptions[8]) ||
                (tableOptions[0] == tableOptions[4] && tableOptions[4] == tableOptions[8]) ||
                (tableOptions[6] == tableOptions[4] && tableOptions[4] == tableOptions[2]))

            {
                _menu.DisplayGameBoard(GetColor(tableOptions), tableOptions);
                PlayerCounter += 1;
                _menu.WinningMessage(CheckPlayersTurnName());
                PlayerCounter = 10;
            }
            else if (PlayerCounter == 10)
            {
                _menu.DisplayGameBoard(GetColor(tableOptions), tableOptions);
                _menu.DrawGameMessage();
                Console.Read();
            }
        }

        public void SetNewGame()
        {
            string userInput;
           
            if (PlayerCounter == 10)
            {
                userInput = _menu.PromptNewGameMessage().ToUpper();
                if (userInput == "Y")
                {
                    for (int i = 1; i < 10; i++)
                    {
                        string letter = i.ToString();
                        tableOptions[i - 1] = Convert.ToChar(letter);
                    }

                    PlayerCounter = 1;
                    this.Start();
                }
                else if (userInput == "N")
                {
                    _menu.EndGameMessage();
                }     
            }    
        }
    }
}
