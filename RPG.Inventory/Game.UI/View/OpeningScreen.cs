﻿using System;
using System.Threading;

namespace MC.DD.UI.View
{
    class OpeningScreen
    {
        public void DisplayOpening()
        {
            
        }

        public void TypeWriter(string text, int speed)
        {
            for (int i = 0; i < text.Length; i++)
            {
                Console.Write(text[i].ToString());
                Thread.Sleep(speed);
            }
        }

    }
}
