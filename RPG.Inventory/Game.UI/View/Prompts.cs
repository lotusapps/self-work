﻿using System;
using MC.DD.Models.Bases;

namespace MC.DD.UI.View
{
    public class Prompts
    {
        public void PromptCharacterMovement()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Press any key to roll dice!\n");
            Console.ForegroundColor = ConsoleColor.White;
            Console.ReadKey();
        }

        public ConsoleKeyInfo PromptCharacterDirections(int movesRemaining)
        {
            Console.Write("Use Directional Keys - Remaining Moves: ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("{0}\n", movesRemaining);
            Console.ForegroundColor = ConsoleColor.White;
            ConsoleKeyInfo input = Console.ReadKey();
            return input;
        }

        public string PromptDecision()
        {
            Console.Write("Enter Command: ");
            Console.ForegroundColor = ConsoleColor.Red;
            string input = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.White;

            return input;
        }

        public void DisplayItems(Character character)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("INVENTORY");
            Console.ForegroundColor = ConsoleColor.White;

            foreach (var item in character.Items)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("{0, 15}", item.Name);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(": {0}", item.Description);
            }
        }
        
        public bool PromptForKeyUsage()
        {
            string input;
            bool invalidInput = true;
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("Gate is locked, Would you like to use Gate Key to open? Y/N : ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Red;
            input = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.White;
            switch (input.ToUpper())
            {
                case "Y":
                    return true;
                case "N":
                default:
                    return false;
            }

        }

        public string PromptForCharacterSelection()
        {

            string Weapon = @"
                        /\                          <>                          <>         
                        \/                       <>(  )<>                       ||
                        ||                      _   <>   _                      ||
                        ||                       \_{  }_/                     /(<>)\
                        ||                          ||                       /  ||  \
                <>====[____]====<>                  ||                          ||  <>
                       |  |                         <>                          ||  ||
                       | ||                         ||                          ||  ||
                       | ||                         ||                          ||/(<>)\
                       | ||                         ||                          |/  ||  \
                       | ||                         ||                          ||  ||
                       | ||                         ||                          ||  ||
                   ____|_||____                _____||_____                _____||__||___
                  ";


            string Wand = @"
                        <>              
                     <>(  )<>    
                    _   <>   _
                     \_{  }_/  
                        ||            
                        ||            
                        <>         
                        ||        
                        ||         
                        ||        
                        ||        
                        ||        
                   _____||_____
                  ";

            string Dagger = @"
                        <>         
                        ||
                        ||
                      /(<>)\
                     /  ||  \
                        ||  <>
                        ||  ||
                        ||  ||
                        ||/(<>)\
                        |/  ||  \
                        ||  ||
                        ||  ||
                   _____||__||___";

            string Sword = @"
                        /\             
                        \/            
                        ||            
                        ||            
                        ||              
                <>====[____]====<>     
                       |  |              
                       | ||                        
                       | ||                       
                       | ||                      
                       | ||                  
                       | ||                  
                   ____|_||____ ";    



            bool InvalidInput = true;
            string result = "";
            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("\t{0}\n", Weapon);

                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("\t\t      Warrior");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("\t\t\t   Mage");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("\t\t\t\tThief\n\n\n");

                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("Select your path... ");

                Console.ForegroundColor = ConsoleColor.Red;
                string input = Console.ReadLine();
                Console.ForegroundColor = ConsoleColor.White;

                switch (input.ToUpper())
                {
                    case "WARRIOR":
                        result = "WARRIOR";
                        InvalidInput = false;
                        break;
                    case "MAGE":
                        result = "MAGE";
                        InvalidInput = false;
                        break;
                    case "THIEF":
                        result = "THIEF";
                        InvalidInput = false;
                        break;
                    default:
                        Console.Clear();
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("\n\n\t\t\tThere is no such hero...");
                        break;
                }
            } while (InvalidInput);
           
            return result;
        }

        public bool PromptForExit()
        {
            string input;
            bool invalidInput = true;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Oh look, a door!, Go in? Y/N : ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Red;
            input = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.White;
            switch (input.ToUpper())
            {
                case "Y":
                    return true;
                case "N":
                default:
                    return false;
            }
        }

        public void GateMessage()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("You've opened the gate.");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("The key broke and disappeared...");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void DoorLockedMessage()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("The door closes and locks right behind you...");
            Console.ForegroundColor = ConsoleColor.White;
            Console.ReadKey();
        }
    }
}
