﻿using System;

namespace MC.DD.UI.View
{
    class InvalidMessages
    {
        public void InvalidDirection()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Can't go in that direction...");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void InvalidDirectionChoice()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("That is not a valid choice...");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void InvalidCommand(string input)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("{0} is not a valid command...", input);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void RequireKeyAccess()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Gate is locked!");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void LockedGateMessage()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Gate is locked! You do not have the required item...");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
