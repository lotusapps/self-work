﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MC.DD.UI.View
{
    public class Commands
    {

        public List<string> MainList { get; set; }
        public List<string> ItemList { get; set; }
        public List<string> FightList { get; set; }
        public List<string> DirectionList { get; set; }
        public List<string> EmptyList { get; set; }
        //  public List<string> UsageList { get; set; }     

        public Commands()
        {
            this.ItemList = new List<string>();
            this.MainList = new List<string>();
            this.FightList = new List<string>();
            this.DirectionList = new List<string>();
            this.EmptyList = new List<string>();

            MainList.Add("MOVE");
            MainList.Add("ITEM");
            MainList.Add("OPTION");
            MainList.Add("");

            FightList.Add("ATTACK");
            FightList.Add("");
            FightList.Add("");
            FightList.Add("");

            ItemList.Add("USE");
            ItemList.Add("EQUIP");
            ItemList.Add("TOSS");
            ItemList.Add("BACK");

           // DirectionList.Add("Directional Key");
            DirectionList.Add("UP");
            DirectionList.Add("DOWN");
            DirectionList.Add("LEFT");
            DirectionList.Add("RIGHT");

            EmptyList.Add("");
            EmptyList.Add("");
            EmptyList.Add("");
            EmptyList.Add("");

        }
    }
}
