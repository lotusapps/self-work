﻿using System;
using System.Threading;

namespace MC.DD.UI.View
{
    class TitleScreen
    {
        private bool Continue = false;
        private bool FirstDraw = true;

        public void DisplayTitle()
        {
            string Start = "Start Game";
            string Quit = "Quit";

            Console.Clear();
            string Heading = @"

         ______  _     _ __   _  ______ _______  _____  __   _      _     _ _______  ______  _____ 
         |     \ |     | | \  | |  ____ |______ |     | | \  |      |_____| |______ |_____/ |     |
         |_____/ |_____| |  \_| |_____| |______ |_____| |  \_|      |     | |______ |    \_ |_____|";


            do
            {
                if (FirstDraw)
                {
                    FirstDraw = false;
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("\n\n     ");
                    TypeWriter(Heading, 2);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("\n\n\n\n\n\t\t\t\t");
                    TypeWriter(Start, 50);
                    Console.Write("\n\t\t\t\t");
                    TypeWriter(Quit, 50);
                    Console.WriteLine("\n\n\n\n\n\n");
                    GetUserInput();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.Write("\n\n     ");
                    Console.Write(Heading);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("\n\n\n\n\n\t\t\t\t");
                    Console.Write(Start);
                    Console.Write("\n\t\t\t\t");
                    Console.Write(Quit);
                    Console.WriteLine("\n\n\n\n\n");
                    InvalidEntryMessage();
                    GetUserInput();
                }
            } while (!Continue);
        }

        public void GetUserInput()
        {
            string userInput;
            bool InvalidInput;

            Console.Write("Type ");
            ColorChange("red", "START ");
            Console.Write("to begin or ");
            ColorChange("red", "QUIT ");
            Console.Write("to exit: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            userInput = Console.ReadLine();
            Console.ForegroundColor = ConsoleColor.White;
            InvalidInput = ValidateAndReturn(userInput);

            if (InvalidInput)
            {
                DisplayTitle();
            }
            else
            {

                Continue = true;
            }
        }

        public bool ValidateAndReturn(string input)
        {
            switch (input.ToUpper())
            {
                case "START":
                    return false;
                case "QUIT":
                    return false;
                default:
                    return true;
            }
        }

        public void InvalidEntryMessage()
        {
            ColorChange("red", "Invalid selection...\n");
        }

        public void TypeWriter(string text, int speed)
        {
            for (int i = 0; i < text.Length; i++)
            {
                if(i > 203)
                    Console.ForegroundColor = ConsoleColor.Yellow;

                Console.Write(text[i].ToString());
                Thread.Sleep(speed);
            }
        }

        private void ColorChange(string color, string text)
        {
            switch (color)
            {
                case "red":
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write(text);
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case "green":
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write(text);
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case "blue":
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write(text);
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case "yellow":
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write(text);
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case "white":
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(text);
                    break;
                default:
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(text);
                    break;
            }
        }
    }
}
