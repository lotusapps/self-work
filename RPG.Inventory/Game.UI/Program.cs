﻿using System;
using MC.DD.UI.Controller;

namespace MC.DD.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetWindowSize(Console.LargestWindowWidth - 55, Console.LargestWindowHeight - 3);

            GameStart game = new GameStart();
            game.Start();

            Console.ReadLine();
        }
    }
}
