﻿using MC.DD.Models.Bases;
using MC.DD.Models.KeyItem;

namespace MC.DD.UI.Controller
{
    public static class KeyCheck
    {
        public static bool CheckForKeys(Character character)
        {
            BossKey key = new BossKey();

            foreach (var i in character.Items)
            {
                if (key.Equals(i))
                    return true;
            }

            return false;
        }

    }
}
