﻿using System;
using System.Collections.Generic;
using MC.DD.Models.Bases;
using MC.DD.UI.Map;
using MC.DD.BLL.Enums;
using MC.DD.BLL.Factory;
using MC.DD.BLL.Game;
using MC.DD.BLL.MapColliders;
using MC.DD.Models.KeyItem;
using MC.DD.UI.View;
using MC.DD.UI.Workflow;

namespace MC.DD.UI.Controller
{
    class GameStart
    {
        // private Player _NewPlayer;
        private MapCollider MapLayout;
        private MapCreator _generateMap;
        private InvalidMessages _Messages;
        private Prompts _Prompts;
        
        private bool test;
        //private KeyCheck _CheckKey;
        private Character Hero;
        private int mapCounter = 1;
        //private List<List<string>> Commands; 

        public GameStart()
        {
            _Messages = new InvalidMessages();
            _Prompts = new Prompts();
            
            MapLayout = new MapCollider("Map1");    
        }
        public void Start()
        {
            TitleScreen title = new TitleScreen();
            title.DisplayTitle();

            //Engine _Game = new Engine();

            //HERO SELECTION
            CharacterSelection _HeroSelection = new CharacterSelection();
            string heroSelection = _Prompts.PromptForCharacterSelection();
            Hero = _HeroSelection.GetCharacter(heroSelection);
            
            //DISPLAY MAIN GAME BOARD
            DisplayGame Display = new DisplayGame();
            Display.SetMap(MapLayout);
            Display.SetCommands(1);
            Display.DisplayGameScreen(Hero);

            BossKey key = new BossKey();
            Hero.Items.Add(key);
            Hero.Items.Add(key);
            Hero.Items.Add(key);
            //Hero.Items.Add(dagger);

            MenuControl Menu = new MenuControl();

            do
            {
                Display.SetCommands(1);
                Menu.Execute(Hero, Display);
            } while (true);

            //Console.ReadLine();
        }

        public int RollDice()

        {
            Random r = new Random();

            return r.Next(10, 15);
        }
    }
}
