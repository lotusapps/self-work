﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MC.DD.BLL.Enums;
using MC.DD.BLL.MapColliders;
using MC.DD.Models.Bases;
using MC.DD.UI.Map;
using MC.DD.UI.View;
using Microsoft.SqlServer.Server;

namespace MC.DD.UI.Controller
{
    public class DisplayGame
    {
        public string MapSelection { get; set; }
        public Dictionary<int, List<string>> _commands { get; set; }
        public List<string> CommandList; 

        private MapCollider _mapCollider;
        private MapCreator _mapCreator;

        public DisplayGame()
        {
            _commands = new Dictionary<int, List<string>>();
            List<string> CommandList = new List<string>();
            Commands commands = new Commands();
            _commands.Add(1, commands.MainList);
            _commands.Add(2, commands.ItemList);
            _commands.Add(3, commands.FightList);
            _commands.Add(4, commands.DirectionList);
            _commands.Add(5, commands.EmptyList);
        }
        public void SetMap(MapCollider map)
        {
            _mapCollider = map;
            _mapCreator = new MapCreator(_mapCollider.Colliders);
        }

        public void DisplayGameScreen(Character hero)
        {
            _mapCreator.GetMapArray(_mapCollider.Colliders);


            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("┌───────────────────────────────────────────────────────────────────────────┐");
            Console.Write("┌──────────────────────────┐\n");

            for (int i = 0; i < 25; i++)
            {
                Console.Write("│");
                Console.ForegroundColor = ConsoleColor.DarkGray;
                for (int j = 0; j < 25; j++)
                {
                    
                    Console.Write("{0,3}", _mapCreator.MapArray[i, j]);
                    
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("│");

                switch (i)
                {
                    case 0:
                        Console.Write("│  {0, -6}         {1, -8} │\n", hero.Name, "LVL: " + hero.Level);
                        break;
                    case 1:
                        Console.Write("├──────────────────────────┤\n");
                        break;
                    case 2:
                        Console.Write("│  {0, -10}              │\n", "HP: " + hero.HP);
                        break;
                    case 3:
                        Console.Write("│  {0, -10}              │\n", "MP: " + hero.MP);
                        break;
                    case 4:
                        Console.Write("│                          │\n");
                        break;
                    case 5:
                        Console.Write("│  {0, -10}{1, -10}    │\n", "ATK: " + hero.Attack, " + 130");
                        break;
                    case 6:
                        Console.Write("│  {0, -10}{1, -10}    │\n", "DEF: " + hero.Defense, " + 300");
                        break;
                    case 7:
                        Console.Write("│                          │\n");
                        break;
                    case 8:
                        Console.Write("│  {0, -12}            │\n", "GOLD: " + hero.Gold);
                        break;
                    case 9:
                        Console.Write("│  {0, -12}            │\n", "EXP: " + hero.EXP);
                        break;
                    case 10:
                        Console.Write("└──────────────────────────┘\n");
                        break;
                    case 11:
                        Console.Write("┌──────────────────────────┐\n");
                        break;
                    case 12:
                        Console.Write("│         ");
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write("{0, -10}", "COMMANDS");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write("       │\n");
                        break;
                    case 13:
                        Console.Write("├──────────────────────────┤\n");
                        break;
                    case 14:
                        Console.Write("│ {0, -20}     │\n", "");
                        break;
                    case 15:
                        Console.Write("│ {0, -20}     │\n", CommandList[0]);
                        break;
                    case 16:
                        Console.Write("│ {0, -20}     │\n", CommandList[1]);
                        break;
                    case 17:
                        Console.Write("│ {0, -20}     │\n", CommandList[2]);
                        break;
                    case 18:
                        Console.Write("│ {0, -20}     │\n", CommandList[3]);
                        break;
                    case 19:
                        Console.Write("│ {0, -20}     │\n", "");
                        break;
                    case 20:
                        Console.Write("│ {0, -20}     │\n", "");
                        break;
                    case 21:
                        Console.Write("│ {0, -20}     │\n", "");
                        break;
                    case 22:
                        Console.Write("│ {0, -20}     │\n", "");
                        break;
                    case 23:
                        Console.Write("│ {0, -20}     │\n", "");
                        break;
                    case 24:
                        Console.Write("│ {0, -20}     │\n", "");
                        break;
                }
            }
            Console.Write("└───────────────────────────────────────────────────────────────────────────┘└──────────────────────────┘\n");
        }

        public void SetCommands(int i)
        {
            CommandList = _commands[i];
        }

        public void SetEnum(int x, int y, ColliderEnum value)
        {
            _mapCollider.Colliders[x, y] = value;
        }

        public ColliderEnum CheckMap(int x, int y)
        {
             var ValueType = _mapCollider.Colliders[x, y];
            
            return ValueType;
        }

        public MapCollider GetCollider()
        {
            return _mapCollider;
        }

        public MapCreator GetMapCreator()
        {
            return _mapCreator;
        }

    }
}
