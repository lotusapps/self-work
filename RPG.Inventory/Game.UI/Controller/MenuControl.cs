﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MC.DD.BLL.MapColliders;
using MC.DD.Models.Bases;
using MC.DD.UI.Map;
using MC.DD.UI.View;
using MC.DD.UI.Workflow;

namespace MC.DD.UI.Controller
{
    public class MenuControl
    {
        private Prompts _prompts;
        private GameStart _game;
        private InvalidMessages _messages;

        public void Execute(Character hero, DisplayGame display)
        {
            _prompts = new Prompts();
            _game = new GameStart();
            _messages = new InvalidMessages();
           
            display.DisplayGameScreen(hero);
            string input = _prompts.PromptDecision();
            
            switch (input.ToUpper())
            {
                case "MOVE":
                    display.SetCommands(4);
                    UserMovement movement = new UserMovement();
                    movement.Execute(hero, display);
                    break;
                case "ITEM":
                    display.SetCommands(5);
                    UserItem itemMenu = new UserItem(display, hero);
                    itemMenu.Execute();
                    break;
                default:
                    //_game.Display();
                    _messages.InvalidCommand(input);
                    break;
            }

        }
      
    }
}
