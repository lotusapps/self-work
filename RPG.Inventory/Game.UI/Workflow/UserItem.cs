﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MC.DD.Models.Bases;
using MC.DD.UI.Controller;
using MC.DD.UI.View;

namespace MC.DD.UI.Workflow
{
    public class UserItem
    {
        private DisplayGame _display;
        private Character _hero;
        private bool _check;

        public UserItem(DisplayGame display, Character hero)
        {
            _display = display;
            _hero = hero;
            _check = true;
        }

        public void Execute()
        {
            do
            {
                _display.SetCommands(5);
                _display.DisplayGameScreen(_hero);
                DisplayItem();
                Item input = CheckInventory(ItemChoicesPrompt());
                if (input != null)
                {
                   
                    PromptItemCommand(input);
                }
                   
                //Console.ReadKey();

            } while (_check);
            
        }

        public void Display()
        {
            _display.DisplayGameScreen(_hero);
        }

        public Item CheckInventory(int item)
        {

            if (item > _hero.Items.Count)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid item choice...");
                Console.ForegroundColor = ConsoleColor.White;
            }
            else if (item == 0)
            {
                _check = false;
            }
            else
            {
                return _hero.Items[item - 1];
            }

            return null;
        }

        public void DisplayItem()
        {
            
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("INVENTORY");
            //Console.ForegroundColor = ConsoleColor.White;

            for (int i = 1; i <= _hero.Items.Count; i++)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write("{0}: ", i);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("{0}",_hero.Items[i-1].Name);
            }

            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("0: ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("BACK");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public void DisplaySelectedItem(Item item)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("{0, 15}\n", item.Name.ToUpper());
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("{0, 15} :  ", "Description");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("{0}", item.Description);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("{0, 15} :  ", "Type");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("{0}", item.Type);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("{0, 15} :  ", "Value");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("{0}", item.Value);
           
        }

        public void PromptItemCommand(Item item)
        {
            string input;
            string returnString = "";
            bool checkValid = true;
            do
            {         
                _display.SetCommands(2);
                _display.DisplayGameScreen(_hero);
                DisplaySelectedItem(item);
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("What would you like to do with the {0} : ", item.Name);
                Console.ForegroundColor = ConsoleColor.Red;
                input = Console.ReadLine();
                Console.ForegroundColor = ConsoleColor.White;
                switch (input.ToUpper())
                {
                    case "USE":
                        returnString = "USE";
                        checkValid = false;
                        break;
                    case "TOSS":
                        TossItem(item);
                        checkValid = false;
                        break;
                    case "EQUIP":
                        returnString = "EQUIP";
                        checkValid = false;
                        break;
                    case "BACK":
                        checkValid = false;
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Not a valid command for {0}", item.Name);
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.ReadKey();
                        break;
                }

            } while (checkValid);

        }

        public void UseItem(Item item)
        {
            
        }

        public void TossItem(Item item)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("Are you sure u want to drop {0}? Y/N :", item.Name);
            Console.ForegroundColor = ConsoleColor.White;
            string input = Console.ReadLine();
            switch (input.ToUpper())
            {
                case "Y":
                    _display.DisplayGameScreen(_hero);
                    _hero.Items.Remove(item);
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("{0}", item.Name);
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(" has been tossed away...");
                    Console.ReadKey();
                    break;
                default:
                    break;
            }
        }

        public  int ItemChoicesPrompt()
        {
            int result;
            string input;

            do
            {
               // _display.DisplayGameScreen(_hero);
                Console.Write("Select Item No. : ");
                input = Console.ReadLine();
                if (Int32.TryParse(input, out result))
                {
                    if (result < 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Not a valid choice...");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    return result;
                }

                //if (input.ToUpper() == "BACK")
                //    return 0;
                else
                {
                    Display();
                    DisplayItem();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("There is no item by that number...");
                    Console.ForegroundColor = ConsoleColor.White;
                }
            } while (true);

        }
    }
}
