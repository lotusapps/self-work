﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MC.DD.BLL.Enums;
using MC.DD.BLL.MapColliders;
using MC.DD.Models.Bases;
using MC.DD.Models.KeyItem;
using MC.DD.UI.Controller;
using MC.DD.UI.Map;
using MC.DD.UI.View;

namespace MC.DD.UI.Workflow
{
    public class UserMovement
    {
        private Prompts _prompts;
       
        private InvalidMessages _messages;
        private int mapCounter = 0;
        private MapCollider _collider;
        private MapCreator _mapCreator;
        private DisplayGame _display;
        private Character _hero;

        public UserMovement()
        {     
            _prompts = new Prompts();           
            
            _messages = new InvalidMessages();
            
        }
        public void Execute(Character hero, DisplayGame display)
        {
            _hero = hero;
            _display = display;
            _prompts.PromptCharacterMovement();
            _collider = display.GetCollider();
            _mapCreator = display.GetMapCreator();

            int diceRoll = RollDice();

            GetDirectionInput(diceRoll);
            
        }

        public void GetDirectionInput(int input)
        {
            int MoveCounter = 0;
            string inputChoice = "";

            Display();

            for (int i = 0; i < input; i++)
            {
                ConsoleKeyInfo movement = _prompts.PromptCharacterDirections(input - i);

                ColliderEnum checkNextLocation;

                switch (movement.Key)
                {
                    case ConsoleKey.UpArrow:
                        checkNextLocation =
                            _collider.Colliders[_mapCreator.player.XCoord - 1, _mapCreator.player.YCoord];
                        if (CheckLocation(checkNextLocation, "UP"))
                            MoveUp();
                        else
                        {
                            i--;
                        }
                        break;
                    case ConsoleKey.DownArrow:
                        checkNextLocation =
                            _collider.Colliders[_mapCreator.player.XCoord + 1, _mapCreator.player.YCoord];
                        if (CheckLocation(checkNextLocation, "DOWN"))
                            MoveDown();
                        else
                        {
                            i--;
                        }
                        break;
                    case ConsoleKey.LeftArrow:
                        checkNextLocation =
                            _collider.Colliders[_mapCreator.player.XCoord, _mapCreator.player.YCoord - 1];
                        if (CheckLocation(checkNextLocation, "LEFT"))
                            MoveLeft();
                        else
                        {
                            i--;
                        }
                        break;
                    case ConsoleKey.RightArrow:
                        checkNextLocation =
                            _collider.Colliders[_mapCreator.player.XCoord, _mapCreator.player.YCoord + 1];
                        if (CheckLocation(checkNextLocation, "RIGHT"))
                            MoveRight();
                        else
                        {
                            i--;
                        }
                        break;
                    default:
                        Display();
                        _messages.InvalidDirectionChoice();
                        i--;
                        break;
                }


            }
        }

        public bool CheckLocation(ColliderEnum location, string dir)
        {
            switch (location)
            {
                case ColliderEnum.Empty:
                    return true;
                case ColliderEnum.Chest:
                    //Fix for CHEST
                    return false;
                case ColliderEnum.Gate:
                    if (KeyCheck.CheckForKeys(_hero) && _prompts.PromptForKeyUsage())
                    {
                        switch (dir.ToUpper())
                        {
                            case "UP":
                                _collider.Colliders[_mapCreator.player.XCoord - 1, _mapCreator.player.YCoord] = ColliderEnum.Empty;
                                break;
                            case "DOWN":
                                _collider.Colliders[_mapCreator.player.XCoord + 1, _mapCreator.player.YCoord] = ColliderEnum.Empty;
                                break;
                            case "LEFT":
                                _collider.Colliders[_mapCreator.player.XCoord, _mapCreator.player.YCoord - 1] = ColliderEnum.Empty;
                                break;
                            case "RIGHT":
                                _collider.Colliders[_mapCreator.player.XCoord, _mapCreator.player.YCoord + 1] = ColliderEnum.Empty;
                                break;
                        }

                        Display();
                        _prompts.GateMessage();
                        _hero.Items.Remove(new BossKey());
                    }
                    else if (!KeyCheck.CheckForKeys(_hero))
                    {
                        Display();
                        _messages.LockedGateMessage();
                    }
                    else
                    {
                        Display();
                        _messages.RequireKeyAccess();
                    }
                    return false;
                case ColliderEnum.Boss:
                    //Fix for BOSS
                    return false;
                case ColliderEnum.Wall:
                    Display();
                    _messages.InvalidDirection();
                    return false;
                case ColliderEnum.End:
                    if (!_prompts.PromptForExit())
                    {
                        Display();
                        return false;
                    }
                   // _mapCreator.ClearMap();
                   // _mapCreator.mapCount += 1;
                    _collider = new MapCollider("Map2");
                    _display.SetMap(_collider);
                    Display();
                    _prompts.DoorLockedMessage();
                    return true;
                default:
                    return false;
            }
        }

        public void MoveUp()
        {
            _collider.Colliders[_mapCreator.player.XCoord, _mapCreator.player.YCoord] = ColliderEnum.Empty;
            _mapCreator.player.XCoord--;
            _collider.Colliders[_mapCreator.player.XCoord, _mapCreator.player.YCoord] = ColliderEnum.Player;
            Display();
        }

        public void MoveDown()
        {
            _collider.Colliders[_mapCreator.player.XCoord, _mapCreator.player.YCoord] = ColliderEnum.Empty;
            _mapCreator.player.XCoord++;
            _collider.Colliders[_mapCreator.player.XCoord, _mapCreator.player.YCoord] = ColliderEnum.Player;
            Display();
        }

        public void MoveLeft()
        {
            _collider.Colliders[_mapCreator.player.XCoord, _mapCreator.player.YCoord] = ColliderEnum.Empty;
            _mapCreator.player.YCoord--;
            _collider.Colliders[_mapCreator.player.XCoord, _mapCreator.player.YCoord] = ColliderEnum.Player;
            Display();
        }

        public void MoveRight()
        {
            _collider.Colliders[_mapCreator.player.XCoord, _mapCreator.player.YCoord] = ColliderEnum.Empty;
            _mapCreator.player.YCoord++;
            _collider.Colliders[_mapCreator.player.XCoord, _mapCreator.player.YCoord] = ColliderEnum.Player;
            Display();
        }


        public int RollDice()
        {
            Random r = new Random();

            return r.Next(10, 15);
        }

        public void Display()
        {
            _display.DisplayGameScreen(_hero);
        }
    }
}
