﻿using System;
using MC.DD.BLL.Enums;
using MC.DD.BLL.MapColliders;
using MC.DD.Models.Bases;

namespace MC.DD.UI.Map
{
    public class MapCreator : Map
    {
        public MapCreator(ColliderEnum[,] character)
        {
            mapCount = 1;
            MapArray = new string[25, 25];
            player = new Player();

            for (int i = 0; i < 25; i++)
            {
                for (int j = 0; j < 25; j++)
                {
                    if(character[i, j] == ColliderEnum.Player)
                    {
                        player.XCoord = i;
                        player.YCoord = j;
                        MapArray[i, j] = " \u25A0 ";   //■
                    }
                }
            }

        }

        public string[,] GetMapArray(ColliderEnum[,] collider)
        {

            for (int i = player.XCoord - 1; i <= player.XCoord + 1; i++)
            {
                for (int j = player.YCoord - 1; j <= player.YCoord + 1; j++)
                {
                    if (collider[i, j] == ColliderEnum.Wall)
                        MapArray[i, j] = "\u2588\u2588\u2588";   //▒▒▒
                    else if (collider[i, j] == ColliderEnum.Empty)
                        MapArray[i, j] = "   ";
                    else if (collider[i, j] == ColliderEnum.Player)
                    {
                        MapArray[i, j] = " \u25A0 ";   //■
                        player.XCoord = i;
                        player.YCoord = j;
                    }                      
                    else if (collider[i, j] == ColliderEnum.Chest)
                        MapArray[i, j] = " \u2640 ";  //?
                    else if (collider[i, j] == ColliderEnum.Boss)
                        MapArray[i, j] = " ! ";
                    else if (collider[i, j] == ColliderEnum.Gate)
                    {
                        if (collider[i + 1, j] == ColliderEnum.Wall && collider[i - 1, j] == ColliderEnum.Wall)
                        {
                            MapArray[i, j] = " \u2551 ";
                        }
                        else
                        {
                            MapArray[i, j] = "\u2550\u2550\u2550";
                        }
                        
                    }
                    else if (collider[i, j] == ColliderEnum.End)
                    {
                        MapArray[i, j] = "[ ]";
                    }

                }

            }

            return MapArray;

            //map.MapArray[player.XCoord, player.YCoord] = player.character;
        }

        public void SetMapArray()
        {
            
        }

        public void ClearMap()
        {
            for (int i = 0; i < 25; i++)
            {
                for (int j = 0; j < 25; j++)
                {

                    MapArray[i, j] = "   ";

                }
            }
        }
    }
}
