﻿using MC.DD.Models.Bases;

namespace MC.DD.UI.Map
{
    public abstract class Map
    {
        public int mapCount { get; set; }
        public string[,] MapArray { get; set; }
        public Player player { get; set; }
    }
}
