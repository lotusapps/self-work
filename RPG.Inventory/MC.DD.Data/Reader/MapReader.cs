﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MC.DD.Data.Reader
{
    public class MapReader
    {
        public int[,] GetMapEnum(string map)
        {
            try
            {
                int[,] MapFile = new int[25, 25];

                using (StreamReader sr = new StreamReader(@"./Maps/" + map + @".dat"))
                {

                    string input;
                    int x = 0;
                    while ((input = sr.ReadLine()) != null)
                    {
                        input = input.Replace(" ", "");
                        for (int index = 0; index < input.Count(); index++)
                        {
                            char c = input[index];
                            MapFile[x, index] = int.Parse(c.ToString());
                        }
                        x++;
                    }

                }
                return MapFile;
            }
            catch (Exception e)
            {

                throw e;
            }

        }
    }
}
