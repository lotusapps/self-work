﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.Enemy
{
    public class Wolf : Character
    {
        public Wolf(int level)
        {
            Name = "Dire Wolf";
            Level = level;
            HP = 60 + RandomizeEnemyHP(level);
            MP = 0;
            Attack = RandomizeAttack(5, 9, level);
            Defense = 0 + RandomizeEnemyDefense(level);
            EXP = 8 + RandomizeEnemyEXP(level);
        }

        public override void BasicAttack()
        {
            throw new System.NotImplementedException();
        }
    }
}
