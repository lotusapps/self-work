﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.Enemy
{
    public class MiniTreant : Character
    {
        public MiniTreant(int level)
        {
            Name = "Mini Treant";
            Level = level;
            HP = 80 + RandomizeEnemyHP(level);
            MP = 30 + RandomizeEnemyMP(level);
            Attack = RandomizeAttack(5, 9, level);
            Defense = 2 + RandomizeEnemyDefense(level);
            EXP = 8 + RandomizeEnemyEXP(level);
            Gold = 8 + RandomizeEnemyGold(level);
        }

        public override void BasicAttack()
        {
            throw new System.NotImplementedException();
        }
    }
}
