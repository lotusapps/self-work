﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.Weapons
{
    public class SteelSword : Weapon
    {
        public SteelSword()
        {
            Name = "Steel Sword";
            Description = "Forged by tiny elves!";
            Weight = 15;
            Value = 150;
            Type = ItemType.Weapon;
            Damage = RandomDamage(14, 18);
        }
    }
}
