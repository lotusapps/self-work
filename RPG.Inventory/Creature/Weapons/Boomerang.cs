﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.Weapons
{
    public class Boomerang : Weapon
    {
        public Boomerang()
        {
            Name = "A Wooden Boomerang";
            Description = "I found this in level 1";
            Weight = 1;
            Value = 5;
            Type = ItemType.Weapon;
            Damage = RandomDamage(1, 4);
        }
    }
}
