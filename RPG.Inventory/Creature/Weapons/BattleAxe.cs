﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.Weapons
{
    public class BattleAxe : Weapon
    {
        public BattleAxe()
        {
            Name = "A Giant Steel Battleaxe";
            Description = "For the glory of Krong!";
            Weight = 10;
            Value = 250;
            Type = ItemType.Weapon;
            Damage = RandomDamage(14, 19);
        }

    }


}
