﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.Weapons
{
    class WoodenStick : Weapon
    {
        public WoodenStick()
        {
            Name = "Tree Branch";
            Description = "Little branch, might be of some use.";
            Weight = 1;
            Value = 1;
            Type = ItemType.Weapon;
            Damage = RandomDamage(1, 3);
        }
    }
}
