﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.Weapons
{
    public class Dagger : Weapon
    {
        public Dagger()
        {
            Name = "Tiny Dagger";
            Description = "Little iron dagger, good for cutting meat.";
            Weight = 2;
            Value = 15;
            Type = ItemType.Weapon;
            Damage = RandomDamage(4, 6);
        }
    }
}
