﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.Weapons
{
    public class WoodenSword : Weapon
    {
        public WoodenSword()
        {
            Name = "A Wooden Sword";
            Description = "It's dangerous to go alone, take this.";
            Weight = 1;
            Value = 10;
            Type = ItemType.Weapon;
            Damage = RandomDamage(2, 4);
        }
    }
}
