﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.Material
{
    public class Mushroom : Item
    {
        public Mushroom()
        {
            Name = "A red mushroom with white spots";
            Description = "Makes you grow larger";
            Weight = 0;
            Value = 1;
            Type = ItemType.Consumable;
        }
    }
}
