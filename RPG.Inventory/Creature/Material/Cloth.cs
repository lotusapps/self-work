﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.Material
{
    public class Cloth : Item
    {
        public Cloth()
        {
            Name = "A piece of cloth";
            Description = "No really, it's just a piece of cloth";
            Weight = 0;
            Value = 10;
            Type = ItemType.Material;
        }
    }
}
