﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.Consumable
{
    class BeefJerky : Consume
    {
        public BeefJerky()
        {
            Name = "Beef Jerky";
            Description = "Dry salty beef jerky, heals you for 15 HP";
            Value = 8;
            Weight = 0;
            Type = ItemType.Consumable;
            Heal = 15;
        }
    }
}
