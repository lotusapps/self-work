﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.Consumable
{
    class GreenHerb : Consume
    {
        public GreenHerb()
        {
            Name = "Green Herb";
            Description = "Nice little plant, heals you for 10 HP.";
            Value = 5;
            Weight = 0;
            Type = ItemType.Consumable;
            Heal = 10;
        }
    }
}
