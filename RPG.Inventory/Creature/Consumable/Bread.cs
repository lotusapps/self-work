﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.Consumable
{
    class Bread : Consume
    {
        public Bread()
        {
            Name = "Bread";
            Description = "Fresh loaf of bread, heals you for 20 HP";
            Value = 15;
            Weight = 0;
            Type = ItemType.Consumable;
            Heal = 20;
        }
    }
}
