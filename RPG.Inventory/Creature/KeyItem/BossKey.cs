﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.KeyItem
{
    public class BossKey : Item
    {
        public BossKey()
        {
            Name = "Gate Key";
            Description = "I wonder what this opens?";
            Weight = 0;
            Value = 5;
            Type = ItemType.Key;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            var OtherKey = obj as BossKey;
            
            if (OtherKey != null)
            {
                if (this.Name != OtherKey.Name)
                    return false;
                if (this.Name == OtherKey.Name)
                    return true;

                return false;
            }
            else
            {
                return false;
            }
        }
    }
}
