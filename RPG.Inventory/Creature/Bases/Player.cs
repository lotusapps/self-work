﻿namespace MC.DD.Models.Bases
{
    public class Player
    {
        public int XCoord { get; set; }
        public int YCoord { get; set; }
        public string character = " \u25A0 ";

        public Player()
        {
          
        }

        public Player(int x, int y)
        {
            XCoord = x;
            YCoord = y;
        }
    }
}
