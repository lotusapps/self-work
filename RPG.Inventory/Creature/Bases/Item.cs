﻿namespace MC.DD.Models.Bases
{
    public abstract class Item
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Weight { get; set; }
        public int Value { get; set; }
        public ItemType Type { get; set; }

        public Item()
        {
            Type = ItemType.Unknown;
        }

        //public override bool Equals(object obj)
        //{
        //    if (obj == null) return false;

        //    var OtherItem = obj as Item;

        //    if (OtherItem != null)
        //    {
        //        if (this.Name != OtherItem.Name)
        //            return false;
        //        if (this.Name == OtherItem.Name)
        //            return true;

        //        return false;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}
    }
}
