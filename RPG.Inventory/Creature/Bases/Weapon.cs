﻿using System;

namespace MC.DD.Models.Bases
{
    public class Weapon : Item
    {
        public int Damage { get; set; }

        protected int RandomDamage(int a, int b)
        {
            int damage = 0;
            var r = new Random();

            damage = r.Next(a, b);
            return damage;
        }
    }
}
