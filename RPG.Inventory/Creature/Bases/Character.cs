﻿using System;
using System.Collections.Generic;

namespace MC.DD.Models.Bases
{
    public abstract class Character/* : ICharacter*/
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public int HP { get; set; }
        public int MP { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int EXP { get; set; }
        public int Gold { get; set; }
        public List<Item> Items { get; set; }

        public abstract void BasicAttack();

        protected int RandomizeAttack(int a, int b, int level)
        {
            int adjustment = (int)((level/2) + level);
            var r = new Random();

            int num = r.Next(a + adjustment, b + adjustment + (int)(level/2));
            return num;
        }

        protected int RandomizeEnemyDefense(int level)
        {
            int adjustment = level;
            return adjustment;
        }

        protected int RandomizeEnemyHP(int level)
        {
            int adjustment = level * 6;
            return adjustment;
        }

        protected int RandomizeEnemyMP(int level)
        {
            int adjustment = level*4;
            return adjustment;
        }

        protected int RandomizeEnemyEXP(int level)
        {
            int adjustment = level*12;
            return adjustment;
        }

        protected int RandomizeEnemyGold(int level)
        {
            int adjustment = level * 7;
            return adjustment;
        }

        //protected int LevelUp()
        //{
            
        //}



        //Attack

        //



    }
}
