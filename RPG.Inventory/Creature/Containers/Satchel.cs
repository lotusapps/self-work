﻿using System;
using MC.DD.Models.Bases;

namespace MC.DD.Models.Containers
{
    public class Satchel : Container
    {
        public Satchel() : base(4)
        {
            Name = "A small cloth satchel";
            Description = "This can carry your knick-knacks.";
            Weight = 1;
            Value = 50;
            Type = ItemType.Container;
        }

        public override void AddItem(Item itemToAdd)
        {
            if(itemToAdd.Weight > 1)
                Console.WriteLine("That is way too big for the satchel!");
            else
               base.AddItem(itemToAdd);
        }


    }
}
