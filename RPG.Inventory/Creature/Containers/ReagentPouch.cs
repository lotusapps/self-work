﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.Containers
{
    public class ReagentPouch: SpecificContainer
    {
        public ReagentPouch() : base(ItemType.Material, 12)
        {
            Name = "A small pouch";
            Description = "This belt pouch could hold a few small materials.";
            Weight = 1;
            Value = 150;
            Type = ItemType.Container;
        }
    }
}
