﻿using MC.DD.Models.Bases;

namespace MC.DD.Models.Containers
{
    public class Backpack : Container
    {
        public Backpack() : base(8)
        {
            Name = "A leather backback";
            Description = string.Format("This is a {0} slot backpack", _capacity);
            Weight = 5;
            Value = 1500;
            Type = ItemType.Container;
        }
    }
}
