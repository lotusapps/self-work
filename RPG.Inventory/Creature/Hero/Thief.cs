﻿using System;
using System.Collections.Generic;
using MC.DD.Models.Bases;

namespace MC.DD.Models.Hero
{
    public class Thief : Character
    { 
        public Thief()
        {
            Name = "Ezio";
            Level = 1;
            HP = 120;
            MP = 40;
            Attack = 8;
            Defense = 4;
            EXP = 0;
            Items = new List<Item>();
        }

        public override void BasicAttack()
        {
            Console.WriteLine("Throws Stars");
        }
    }
}
