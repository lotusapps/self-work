﻿using System;
using System.Collections.Generic;
using MC.DD.Models.Bases;

namespace MC.DD.Models.Hero
{
    public class Mage : Character
    { 
        public Mage()
        {
            Name = "Lina";
            Level = 1;
            HP = 100;
            MP = 80;
            Attack = 9;
            Defense = 2;
            EXP = 0;
            Items = new List<Item>();
        }

        public override void BasicAttack()
        {
            Console.WriteLine("Shoots Fireball");
        }
    }
}
