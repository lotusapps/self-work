﻿using System;
using System.Collections.Generic;
using MC.DD.Models.Bases;


namespace MC.DD.Models.Hero
{
    public class Warrior : Character
    {
        public Warrior()
        {
            Name = "Sven";
            Level = 1;
            HP = 150;
            MP = 20;
            Attack = 7;
            Defense = 6;
            EXP = 0;
            Items = new List<Item>();
        }

        public override void BasicAttack()
        {
            Console.WriteLine("Swing Sword");
        }
    }
}
