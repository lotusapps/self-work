﻿namespace MC.DD.Models
{
    public enum ItemType
    {
        Unknown,
        Material,
        Weapon,
        Container,
        Consumable,
        Key
    }
}
