﻿namespace MC.DD.BLL.Enums
{
    public enum ColliderEnum
    {
        Wall,
        Empty,
        Chest,
        Boss,
        Gate,
        Player,
        End
    }
}
