﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MC.DD.BLL.Factory;
using MC.DD.Models.Bases;

namespace MC.DD.BLL.Game
{
    public class Engine
    {
        private Player _player;

        public Engine()
        {
            _player = new Player(23, 20);
        }

        public int RollDice()
        {
            Random r = new Random();

            return r.Next(1, 7);
        }

        //public void Start(Character hero)
        //{
        //    //Get Hero
        //    Hero = hero;
        //}
    }
}
