﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MC.DD.BLL.Enums;
using MC.DD.Data;
using MC.DD.Data.Reader;

namespace MC.DD.BLL.MapColliders
{
    public class MapCollider
    {
        public ColliderEnum[,] Colliders { get; set; }

        public MapCollider(string map)
        {
            MapReader GetMap = new MapReader();
            int[,] MapInt = GetMap.GetMapEnum(map);

            Colliders = new ColliderEnum[25, 25];

            for (int i = 0; i < 25; i++)
            {
                for (int j = 0; j < 25; j++)
                {
                    Colliders[i, j] = (ColliderEnum)MapInt[i, j];
                }
            }
        }
    }
}
