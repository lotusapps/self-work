﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MC.DD.Models.Bases;
using MC.DD.Models.Hero;

namespace MC.DD.BLL.Factory
{
    public class CharacterSelection
    {
        public Character GetCharacter(string input)
        {
            switch (input)
            {
                case "WARRIOR":
                    return new Warrior();
                case "MAGE":
                    return new Mage();
                case "THIEF":
                    return new Thief();
                default:
                    return null;
            }
        }
    }
}
