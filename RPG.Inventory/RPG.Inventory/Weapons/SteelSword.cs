﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Weapons
{
    public class SteelSword : Weapon
    {
        public SteelSword()
        {
            Name = "Steel Sword";
            Description = "Forged by tiny elves!";
            Weight = 15;
            Value = 150;
            Type = ItemType.Weapon;
            Damage = RandomDamage(14, 18);
        }
    }
}
