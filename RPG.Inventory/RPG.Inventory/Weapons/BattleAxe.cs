﻿using System;
using RPG.Inventory.Base;

namespace RPG.Inventory.Weapons
{
    public class BattleAxe : Weapon
    {
        public BattleAxe()
        {
            Name = "A Giant Steel Battleaxe";
            Description = "For the glory of Krong!";
            Weight = 10;
            Value = 250;
            Type = ItemType.Weapon;
            Damage = RandomDamage(14, 19);
        }

    }


}
