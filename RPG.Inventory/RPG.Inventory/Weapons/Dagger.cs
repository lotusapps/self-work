﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Weapons
{
    public class Dagger : Weapon
    {
        public Dagger()
        {
            Name = "Tiny Dagger";
            Description = "Little iron dagger, good for cutting meat.";
            Weight = 2;
            Value = 15;
            Type = ItemType.Weapon;
            Damage = RandomDamage(4, 6);
        }
    }
}
