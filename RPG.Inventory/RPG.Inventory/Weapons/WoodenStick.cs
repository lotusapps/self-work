﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Weapons
{
    class WoodenStick : Weapon
    {
        public WoodenStick()
        {
            Name = "Tree Branch";
            Description = "Little branch, might be of some use.";
            Weight = 1;
            Value = 1;
            Type = ItemType.Weapon;
            Damage = RandomDamage(1, 3);
        }
    }
}
