﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Consumable
{
    class BeefJerky : Consume
    {
        public BeefJerky()
        {
            Name = "Beef Jerky";
            Description = "Dry salty beef jerky, heals you for 15 HP";
            Value = 8;
            Weight = 0;
            Type = ItemType.Consumable;
            Heal = 15;
        }
    }
}
