﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Consumable
{
    class GreenHerb : Consume
    {
        public GreenHerb()
        {
            Name = "Green Herb";
            Description = "Nice little plant, heals you for 10 HP.";
            Value = 5;
            Weight = 0;
            Type = ItemType.Consumable;
            Heal = 10;
        }
    }
}
