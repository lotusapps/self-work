﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPG.Inventory.Base;

namespace RPG.Inventory.Consumable
{
    class Bread : Consume
    {
        public Bread()
        {
            Name = "Bread";
            Description = "Fresh loaf of bread, heals you for 20 HP";
            Value = 15;
            Weight = 0;
            Type = ItemType.Consumable;
            Heal = 20;
        }
    }
}
