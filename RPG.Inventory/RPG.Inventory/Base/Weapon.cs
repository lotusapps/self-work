﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG.Inventory.Base
{
    public class Weapon : Item
    {
        public int Damage { get; set; }

        protected int RandomDamage(int a, int b)
        {
            int damage = 0;
            var r = new Random();

            damage = r.Next(a, b);
            return damage;
        }
    }
}
