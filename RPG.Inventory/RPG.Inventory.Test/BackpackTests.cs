﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MC.DD.Models.Containers;
using MC.DD.Models.Material;
using MC.DD.Models.Weapons;
using NUnit.Framework;

namespace RPG.Inventory.Test
{
    [TestFixture]
    public class BackpackTests
    {
        [Test]
        public void PutItemInBackpackTest()
        {
            //Arrange
            Backpack bag = new Backpack();
            BattleAxe axe = new BattleAxe();
            Mushroom shroom = new Mushroom();
            Satchel sat = new Satchel();

            //Act
            bag.AddItem(axe);
            bag.AddItem(shroom);
            bag.AddItem(sat);

            //Assert
            Assert.AreEqual(3, bag.ItemCount);

            bag.DisplayContents();
        }

        [Test]
        public void PutItemInSatchelTest()
        {
            //arrange
            Satchel satchel = new Satchel();
            BattleAxe axe = new BattleAxe();

            //act
            satchel.AddItem(axe);

            //assert
            Assert.AreEqual(0, satchel.ItemCount);
        }

        [Test]
        public void PutItemInSatchelTestAdd()
        {
            //arrange
            Satchel satchel = new Satchel();
            Boomerang boom = new Boomerang();

            //act
            satchel.AddItem(boom);

            //assert
            Assert.AreEqual(1, satchel.ItemCount);
        }
    }
}
