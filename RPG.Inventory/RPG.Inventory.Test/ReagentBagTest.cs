﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MC.DD.Models.Containers;
using MC.DD.Models.Weapons;
using NUnit.Framework;

namespace RPG.Inventory.Test
{
    [TestFixture]
    class ReagentBagTest
    {
        [Test]
        public void CannotAddNonReagentTest()
        {
            //arrange
            ReagentPouch pouch = new ReagentPouch();
            BattleAxe axe = new BattleAxe();

            //act
            pouch.AddItem(axe);

            //assert
            Assert.AreEqual(0, pouch.ItemCount);
        }
    }
}
