USE Northwind
GO

--max price for each product group by category
SELECT MAX(UnitPrice)
FROM Products
GROUP BY CategoryID

--List of customer with most orders submitted and total amount for the orders
SELECT Count(Orders.OrderID) AS NumbersOfOrder, SUM([Order Details].UnitPrice) AS TotalAmount
FROM Customers
INNER JOIN Orders
ON Customers.CustomerID = Orders.CustomerID
INNER JOIN [Order Details]
ON Orders.OrderID = [Order Details].OrderID
GROUP BY Customers.CustomerID

--customers with more than 10 orders
SELECT ContactName, COUNT(Orders.OrderID) AS NumOfOrders
FROM Customers
INNER JOIN Orders
ON Customers.CustomerID = Orders.CustomerID
GROUP BY ContactName
HAVING COUNT(Orders.OrderID) > 10

--Employees with more than 100 orders
SELECT LastName + ',' + FirstName AS EmployeeName, COUNT(o.OrderID) AS OrderCount 
FROM Employees e
INNER JOIN Orders o
ON e.EmployeeID = o.EmployeeID
GROUP BY LastName, FirstName
HAVING COUNT(o.OrderID) > 100

