USE SWCCorp
GO

--Every combination of employee and location
SELECT * 
FROM Employee e
FULL OUTER JOIN Location l
ON e.LocationID = l.LocationID

--All employees with no grant
SELECT * 
FROM Employee e
LEFT JOIN [Grant] g
ON e.EmpID = g.EmpID
WHERE GrantID IS NULL

--Select first to view
SELECT *
FROM Employee
WHERE EmpID = 11

--Update last name of sally
UPDATE Employee
SET LastName = 'Green'
WHERE EmpID = 11

--list of everyone in Spokane
SELECT *
FROM Employee e
INNER JOIN Location l
ON e.LocationID = l.LocationID
WHERE City = 'Spokane'

--Update employee in spokane status to External
UPDATE e
SET [Status] = 'External'
FROM Employee e
INNER JOIN Location l
ON e.LocationID = l.LocationID
WHERE City = 'Spokane'

--Get Location table
SELECT * 
FROM Location

--Update street name
UPDATE Location
SET Street = '111 1st Ave'
WHERE LocationID = 1

--Get record
SELECT * 
FROM Employee e
INNER JOIN [Grant] g
ON e.EmpID = g.EmpID
INNER JOIN Location l
ON e.LocationID = l.LocationID
WHERE City = 'Boston'

--Change grant to 20k
UPDATE g
SET Amount = 20000
FROM Employee e
INNER JOIN [Grant] g
ON e.EmpID = g.EmpID
INNER JOIN Location l
ON e.LocationID = l.LocationID
WHERE City = 'Boston'





