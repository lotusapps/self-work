USE Northwind
GO

--ALL PRODUCTS
SELECT *
FROM Products

SELECT *
FROM Products
WHERE productName LIKE 'Queso Cabrales'

SELECT productName, unitsInStock
FROM Products
WHERE ProductName IN ('Laughing Lumberjack Lager' , 'Outback Lager','Ravioli Angelo')

SELECT *
FROM Orders
WHERE CustomerID = 'QUEDE'

SELECT *
FROM Orders
WHERE Freight > 100.00

SELECT *
FROM Orders
WHERE Freight BETWEEN 10.00 AND 20.00 AND ShipCountry = 'USA'

SELECT firstName, lastName, TerritoryDescription 
FROM Employees
INNER JOIN EmployeeTerritories
ON Employees.EmployeeID = EmployeeTerritories.EmployeeID
INNER JOIN Territories
ON EmployeeTerritories.TerritoryID = Territories.TerritoryID

SELECT contactName, orderDate, unitPrice, Quantity, Discount, country
FROM Customers
INNER JOIN Orders
ON Customers.CustomerID = Orders.CustomerID
INNER JOIN [Order Details]
ON Orders.OrderID = [Order Details].OrderID
WHERE Customers.Country = 'USA'

SELECT ProductName, CompanyName
FROM Products
INNER JOIN Suppliers
ON Products.SupplierID = Suppliers.SupplierID
WHERE Products.ProductName = 'chai'

SELECT contactName, orderDate, Quantity, Discount, productName
FROM Customers
INNER JOIN Orders
ON Customers.CustomerID = Orders.CustomerID
INNER JOIN [Order Details]
ON Orders.OrderID = [Order Details].OrderID
INNER JOIN Products
ON [Order Details].ProductID = products.ProductID
WHERE Products.ProductName = 'chai'