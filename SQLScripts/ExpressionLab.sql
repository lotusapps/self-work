USE Northwind
GO

--list of products values of stock highest to lowest
SELECT (UnitPrice * UnitsInStock) AS ValueOfStock
FROM Products
ORDER BY ValueOfStock DESC

--Format last name and first name and order by
SELECT LastName + ' ' + FirstName AS NameLastFirst
FROM Employees
ORDER BY LastName, FirstName


SELECT (UnitPrice * UnitsInStock) AS ValueOfStock,
((UnitPrice * UnitsInStock) * 1.38) AS Canadian,
((UnitPrice * UnitsInStock) * 112.83) AS JapaneseYen,
((UnitPrice * UnitsInStock) * 0.9) AS Euro,
((UnitPrice * UnitsInStock) * 18.3) AS Peso
FROM Products
ORDER BY ValueOfStock DESC