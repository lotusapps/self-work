USE MovieCatalog
GO

ALTER TABLE Movies
ADD [ReleaseYear] INT NULL

ALTER TABLE Movies
ADD [Teaser] NVARCHAR(50) NOT NULL
DEFAULT 'default'

EXEC sp_rename 'RunningTime', 'RuntimeMinutes'