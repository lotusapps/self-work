CREATE PROCEDURE GetProductListByCategory
(
	@CategoryName NVARCHAR(20)
)AS

SELECT *
FROM CurrentProducts
WHERE Category = @CategoryName

GO

CREATE PROCEDURE GetGrantsByEmployee
(
	@LastName NVARCHAR(20)
)AS

SELECT Employee.EmpID, FirstName, LastName
FROM Employee
INNER JOIN [Grant]
ON Employee.HireDate = [Grant].EmpID
WHERE LastName = @LastName

DECLARE @GrantID NCHAR(5)
SET @GrantID = 5

UPDATE [Grant]
SET GrantName = 'Nothing'
WHERE GrantID = @GrantID

UPDATE [Grant]
SET GrantName = 'Nothing'
WHERE GrantID = @GrantID