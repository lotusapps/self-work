USE SWCCorp
GO

--2 oldest employee in the table
SELECT TOP(2) WITH TIES *
FROM Employee
ORDER BY HireDate ASC

--top 6 largest grant with ties
SELECT TOP(6) WITH TIES *
FROM [Grant]
ORDER BY Amount DESC

--10 most expensive single-day trips
--THERE ARE NO 1 NIGHTS
SELECT TOP(10) *
FROM CurrentProducts
WHERE Category = 'No-Stay'
ORDER BY RetailPrice DESC

SELECT *
FROM CurrentProducts



