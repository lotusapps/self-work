USE Northwind
GO

--Query for customer and order information for AROUT
SELECT *
FROM Customers
INNER JOIN Orders
ON Customers.CustomerID = Orders.CustomerID
WHERE Customers.CustomerID = 'AROUT'

-- combine orders, order details, and products
SELECT Orders.OrderID, OrderDate, [Order Details].UnitPrice, Products.UnitPrice, Quantity, Discount, ProductName
FROM Orders
INNER JOIN [Order Details]
ON Orders.OrderID = [Order Details].OrderID
INNER JOIN Products
ON [Order Details].ProductID = Products.ProductID