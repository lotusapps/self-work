USE SWCCorp
GO

--Alphabetical order by name
SELECT * 
FROM [Grant]
ORDER BY GrantName

--ordery by hire date newest to oldest
SELECT * 
FROM Employee
ORDER BY HireDate DESC

--order by retail price most expensive to least
SELECT ProductName, Category, RetailPrice
FROM CurrentProducts
ORDER BY RetailPrice DESC

--order by amount and if there is a tie order by name
SELECT * 
FROM [Grant]
ORDER BY Amount DESC, GrantName

--order by city null show first
SELECT FirstName, LastName, City
FROM Employee
FULL OUTER JOIN Location
ON Employee.LocationID = Location.LocationID
ORDER BY City