﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;

namespace BattleShip.UI.View
{
    public class Menu
    {
        public string PromptPlayername(int playerNumber)
        {
            string playerName = "";
            Console.Write("Captain {0}'s name: ", playerNumber);
            playerName = Console.ReadLine();
            return playerName;
        }

        public void TitleDisplay()
        {
            Console.Clear();
            string Boat = @"                                      @@@@        
                                     @@@                  _____
                                   _@@_         _________|_____|_ 
                                  |____|       |__________________\
                                  |    |       |                    \
                                  |    |       |     O     O     O   |
                __________________|____|_______|_____________________|_________
                \_____________________________________________________________/
                 \                                                           /
                  \     B A T T L E S H I P                                 /
                   \                                                       /
                    \                                                     /";
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(Boat);
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        public void promtForShipPlacement(string ship)
        {
            Console.Write("Pick a location for your ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("{0}", ship);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public string promptShipCoordinate()
        {
            string coord;
            Console.Write("Enter Coordinates: ex. B5: ");
            coord = Console.ReadLine();
            return coord;
        }

        public string promptShipShotCoordinate()
        {
            string coord;
            Console.Write("Enter Shot Coordinates: ex. A5: ");
            coord = Console.ReadLine();
            return coord;
        }

        public string promtDirection()
        {
            string userInput;
            Console.Write("Direction of ship? (U)p, (D)own, (L)eft, or (R)ight? ");
            userInput = Console.ReadLine();
            return userInput.ToUpper();
        }

        public string HitSinkMessages(string ship)
        {
           return string.Format("You hit AND sunk the opponents {0}", ship);
        }

        public string HitMessage()
        {
            return string.Format("You hit one of your opponents ships!");
        }

        public string MissMessage()
        {
            return string.Format("You fire a shot and it misses!");
        }

        public string RepeatMessage()
        {
            return string.Format("You have already fired a shot there! \t Please choose new coordinates!");
        }

        public string InvalidMessage()
        {
            return string.Format("That spot is invalid! Pleaase pick correct coordinate (A-J) and (1-10)");
        }

        public string WinnerMessage(string name)
        {
            return string.Format("Congrats {0} you win!!!!!", name);
        }

        public void InvalidCoordinates()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Invalid coordinates, ");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public bool PlayAgain()
        {
            string answer;
            Console.WriteLine("Would you like to play again? Y/N");
            answer = Console.ReadLine();
            switch (answer.ToUpper())
            {
                case "Y":
                    return true;
                    break;
                case "N":
                    return false;
                    break;
                default:
                    this.PlayAgain();
                    break;
            }
            return true;
        }

        public void PlayerTurn(int player)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\t\t\t\t\t  Player {0}'s Turn",player );
            Console.ForegroundColor = ConsoleColor.White;
        }

}

}
