﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.Requests;
using BattleShip.UI.Workflow;


namespace BattleShip.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            bool newGame = true;
            Console.SetWindowPosition(0,0);
            Console.SetWindowSize(Console.LargestWindowWidth - 40, Console.LargestWindowHeight - 5);

            do
            {
                 TheGame Game= new TheGame();
                 Game.StartGame();

                newGame = Game.NewGame();
            } while (newGame);
            Console.Clear();
            Console.WriteLine("Goodbye");
            Console.ReadLine();

        }
    }
}
