﻿using System;
using System.Collections.Generic;
using BattleShip.BLL.GameLogic;
using BattleShip.UI.View;
using BattleShip.BLL.Model;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;
using BattleShip.UI.Model;
using System.Media;

namespace BattleShip.UI.Workflow
{

    class TheGame
    {
        SoundPlayer hitandsink = new SoundPlayer(@"..\..\SFX\hitandsink.wav");
        SoundPlayer splash = new SoundPlayer(@"..\..\SFX\splash.wav");
        SoundPlayer hit = new SoundPlayer(@"..\..\SFX\explosion.wav");
        private int Counter = 1;
        List<Board> PlayerBoards = new List<Board>();
        List<Player> PlayerList = new List<Player>();
        Ship[] allShips = new Ship[5];
        Menu _menu = new Menu();
        TheBoard DrawGrid = new TheBoard();
        ConsoleColor[,] allColors1 = new ConsoleColor[10, 10];
        ConsoleColor[,] allColors2 = new ConsoleColor[10, 10];
        private bool winner = false;

        public void StartGame()
        {
          
                _menu.TitleDisplay();
                createBoard();
                GetPlayerName();
                GetShipPlacement();
                do
                {
                    PlayGame();
                } while (winner == false);
            

                
        }

        public void PlayGame()
        {
            char[,] Player1ShotHistory = new char[10,10];
            char[,] Player2ShotHistory = new char[10, 10];

            FireShotResponse shotResponse = new FireShotResponse();
            string message = "";

            do
            {           
                for (int i = 0; i <= 1; i++)
                {
                    int X = 0;
                    int Y = 0;
                    int[] XYShots = new int[2];

                    do
                    {
                        if (winner)
                            break;

                        DrawGrid.DrawBoard(Player1ShotHistory, Player2ShotHistory, i + 1, allColors1, allColors2, message);

                        XYShots = GetShipCoordinates(_menu.promptShipShotCoordinate());
                        Coordinate coord = new Coordinate(XYShots[0], XYShots[1]);
                        shotResponse = PlayerBoards[i].FireShot(coord);

                        if (shotResponse.ShotStatus == ShotStatus.Duplicate)
                        {
                            message = _menu.RepeatMessage();
                        }
                        else if (shotResponse.ShotStatus == ShotStatus.Invalid)
                        {
                           message = _menu.InvalidMessage();
                        }

                    } while (shotResponse.ShotStatus == ShotStatus.Duplicate || shotResponse.ShotStatus == ShotStatus.Invalid);
                    if (shotResponse.ShotStatus == ShotStatus.HitAndSunk)
                    {
                        string ship = shotResponse.ShipImpacted;
                        message = _menu.HitSinkMessages(ship);
                        if (i == 0)
                        {
                            hitandsink.Play();
                            Player1ShotHistory[XYShots[1] - 1, XYShots[0] - 1] = 'H';
                            allColors1[XYShots[1] - 1, XYShots[0] - 1] = ConsoleColor.Red;
                        }
                        else if (i == 1)
                        {
                            hitandsink.Play();
                            Player2ShotHistory[XYShots[1] - 1, XYShots[0] - 1] = 'H';
                            allColors2[XYShots[1] - 1, XYShots[0] - 1] = ConsoleColor.Red;
                        }

                    }
                    else if (shotResponse.ShotStatus == ShotStatus.Hit)
                    {
                       message = _menu.HitMessage();
                        if (i == 0)
                        {
                            hit.Play();
                            Player1ShotHistory[XYShots[1] - 1, XYShots[0] - 1] = 'H';
                            allColors1[XYShots[1] - 1, XYShots[0] - 1] = ConsoleColor.Red;
                        }
                        else if(i == 1)
                        {
                            hit.Play();
                            Player2ShotHistory[XYShots[1] - 1, XYShots[0] - 1] = 'H';
                            allColors2[XYShots[1] - 1, XYShots[0] - 1] = ConsoleColor.Red;
                        }
                    }
                    else if (shotResponse.ShotStatus == ShotStatus.Miss)
                    {
                       message =_menu.MissMessage();
                        if (i == 0)
                        {
                            splash.Play();
                            Player1ShotHistory[XYShots[1] - 1, XYShots[0] - 1] = 'M';
                            allColors1[XYShots[1] - 1, XYShots[0] - 1] = ConsoleColor.Yellow;
                        }
                        else if (i == 1)
                        {
                            splash.Play();
                            Player2ShotHistory[XYShots[1] - 1, XYShots[0] - 1] = 'M';
                            allColors2[XYShots[1] - 1, XYShots[0] - 1] = ConsoleColor.Yellow;
                        }
                        
                    }

                    if (shotResponse.ShotStatus == ShotStatus.Victory)
                    {
                        if (winner)
                            break;
                        message = _menu.WinnerMessage(PlayerList[i].PlayerName);
                        winner = true;
                    }

                    DrawGrid.DrawBoard(Player1ShotHistory, Player2ShotHistory, i + 1, allColors1, allColors2, message);
                    Console.WriteLine("Press any key to continue!");
                    Console.ReadKey();


                }
                
            } while (shotResponse.ShotStatus != ShotStatus.Victory);
                    
        }

        public void GetPlayerName()
        {
            for (int i = 1; i <= 2; i++)
            {
                Player P = new Player();
                P.PlayerName = _menu.PromptPlayername(i);
                PlayerList.Add(P);
            }
        }

        public int GetCoord(string str)
        {
            int Coordinate = 0;
            string word = str;
            switch (word.ToUpper())
            {
                case "A":
                    Coordinate = 1;
                    break;
                case "B":
                    Coordinate = 2;
                    break;
                case "C":
                    Coordinate = 3;
                    break;
                case "D":
                    Coordinate = 4;
                    break;
                case "E":
                    Coordinate = 5;
                    break;
                case "F":
                    Coordinate = 6;
                    break;
                case "G":
                    Coordinate = 7;
                    break;
                case "H":
                    Coordinate = 8;
                    break;
                case "I":
                    Coordinate = 9;
                    break;
                case "J":
                    Coordinate = 10;
                    break;
                default:
                    _menu.InvalidCoordinates();
                    _menu.promptShipCoordinate();
                    break;
            }
            return Coordinate;

            
        }

        public void createBoard()
        {
            Board Board1 = new Board();
            Board Board2 = new Board();
            PlayerBoards.Add(Board1);
            PlayerBoards.Add(Board2);
        }


        public void GetShipPlacement()
        {
            ShipPlacement placement = new ShipPlacement();
           
            for (int j = 0; j < 2; j++)
            {
                Console.Clear();
                 int shipCounter = 0;
                DrawGrid.DrawBoardWithShip(PlayerBoards[j].GetShips(), shipCounter, j + 1);            

                for (int i = 0; i < 5; i++)
                {
                    do
                    {

                        PlaceShipRequest shipRequest = new PlaceShipRequest();
                        
                        string[] ships = Enum.GetNames(typeof (ShipType));

                        _menu.promtForShipPlacement(ships[i]);
                       
                        switch (i)
                        {
                            case 0:
                                shipRequest.ShipType = ShipType.Destroyer;
                                break;
                            case 1:
                                shipRequest.ShipType = ShipType.Submarine;
                                break;
                            case 2:
                                shipRequest.ShipType = ShipType.Carrier;
                                break;
                            case 3:
                                shipRequest.ShipType = ShipType.Battleship;
                                break;
                            case 4:
                                shipRequest.ShipType = ShipType.Cruiser;
                                break;
                        }

                        int[] XYCoords = GetShipCoordinates(_menu.promptShipCoordinate());
                        Coordinate coordinate = new Coordinate(XYCoords[0], XYCoords[1]);
                        shipRequest.Coordinate = coordinate;

                        switch (_menu.promtDirection())
                        {
                            case "U":
                                shipRequest.Direction = ShipDirection.Up;
                                break;
                            case "D":
                                shipRequest.Direction = ShipDirection.Down;
                                break;
                            case "L":
                                shipRequest.Direction = ShipDirection.Left;
                                break;
                            case "R":
                                shipRequest.Direction = ShipDirection.Right;
                                break;
                        }

                        placement = PlayerBoards[j].PlaceShip(shipRequest);

                    } while (placement == ShipPlacement.NotEnoughSpace || placement == ShipPlacement.Overlap);

                    DrawGrid.DrawBoardWithShip(PlayerBoards[j].GetShips(),shipCounter, j+1);
                    shipCounter++;
                }         
            }
        }

        public int[] GetShipCoordinates(string coord)
        {
            int[] coordinate = new int[2];
            bool InCorrectInput = true;
            bool validInput = false;
            do
            {

                if (coord == "")
                {
                    _menu.InvalidCoordinates();
                    coord = _menu.promptShipCoordinate();

                }
                else
                {
                    coordinate[0] = GetCoord(coord[0].ToString());
                    validInput = int.TryParse(coord.Substring(1, coord.Length - 1), out coordinate[1]);

                    if (coord.Length > 2 && validInput == false)
                    {
                        _menu.InvalidCoordinates();
                        coord = _menu.promptShipCoordinate();
                    }
                    else
                    {
                        InCorrectInput = false;
                    }
                }

            } while (InCorrectInput);
            
            return coordinate;
        }

        public bool NewGame()
        {
            return _menu.PlayAgain();

        }

    }//class
}//namespace
