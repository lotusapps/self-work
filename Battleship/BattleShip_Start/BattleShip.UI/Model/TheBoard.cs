﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Ships;
using BattleShip.UI.View;

namespace BattleShip.UI.Model
{
    class TheBoard
    {
        private Menu _menu = new Menu();
        Coordinate[] coord;
        string[,] BoatPosition = new string[10, 10];
        string[,] BoatPosition2 = new string[10,10];
        char[] boardLetters = new char[] { '\0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J' };

        //Game Board during the game
        public void DrawBoard(char[,] p1Board, char[,] p2Board, int playerTurn, ConsoleColor[,] colors1, ConsoleColor[,] colors2, string message = "")    
        {
            Console.Clear();
            char[,] Player1Board = p1Board;
            char[,] Player2Board = p2Board;
      
            int counter = 1;

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\t\t╔═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╗");
            Console.Write("\t\t");
            for (int i = 0; i <= 11; i++)
            {

                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("║  ");
                Console.ForegroundColor = ConsoleColor.White;
                if (i <= 10)
                {
                    Console.Write("{0}", boardLetters[i]);
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("  ");
                Console.ForegroundColor = ConsoleColor.Blue;
            }

            Console.WriteLine("\n\t\t╠═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╣");

            for (int i = 0; i < 10; i++)
            {

                Console.Write("\t\t");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("║  ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("{0, 2}", counter);
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write(" ");
                Console.ForegroundColor = ConsoleColor.Gray;
                counter++;

                for (int j = 0; j < 11; j++)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("║  ");
                    Console.ForegroundColor = ConsoleColor.White;

                    if (j < 10)
                    {
                        if (playerTurn == 1)
                        {
                            Console.ForegroundColor = colors1[i, j];
                            Console.Write("{0,1}", Player1Board[i, j]);
                        }
                        else if (playerTurn == 2)
                        {
                            Console.ForegroundColor = colors2[i, j];
                            Console.Write("{0,1}", Player2Board[i, j]);
                        }
                    }
                   
                    Console.Write("  ");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                if (i < 9)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("\n\t\t╠═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╣\n");
                    Console.ForegroundColor = ConsoleColor.Gray;
                }

                if (i == 9)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine("\n\t\t╚═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╝\n");
                    Console.ForegroundColor = ConsoleColor.Gray;
                }

            }
            _menu.PlayerTurn(playerTurn);
            Console.WriteLine("{0}", message);        
        }

        //Game board at beginning of game to see ship placements
        public void DrawBoardWithShip(Ship[] ships, int shipCounter, int player)
        {
           Console.Clear();
                
           Ship[] player1Ships = ships;
           Ship[] player2Ships = ships;

            if (player == 1)
            {
                for (int i = 0; i <= shipCounter; i++)
                {
                    if (player1Ships[i] != null)
                    {
                        coord = player1Ships[i].BoardPositions;
                        for (int j = 0; j < coord.Length; j++)
                        {
                            int yCoord = coord[j].YCoordinate;
                            int xCoord = coord[j].XCoordinate;

                            BoatPosition[yCoord - 1, xCoord - 1] = "■";
                        }
                    }
                }
            }else if (player == 2)
            {
                for (int i = 0; i <= shipCounter; i++)
                {
                    if (player2Ships[i] != null)
                    {
                        coord = player2Ships[i].BoardPositions;
                        for (int j = 0; j < coord.Length; j++)
                        {
                            int yCoord = coord[j].YCoordinate;
                            int xCoord = coord[j].XCoordinate;

                            BoatPosition2[yCoord - 1, xCoord - 1] = "■";
                        }
                    }
                }
            }
           
            int counter = 1;

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("\t\t╔═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╦═════╗");
            Console.Write("\t\t");
            for (int i = 0; i <= 11; i++)
            {             
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("║  ");
                Console.ForegroundColor = ConsoleColor.White;
                if (i <= 10)
                {
                    Console.Write("{0}", boardLetters[i]);
                }   
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("  ");
                Console.ForegroundColor = ConsoleColor.Blue;

            }

            Console.WriteLine("\n\t\t╠═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╣");

            for (int i = 0; i < 10; i++)
            {

                Console.Write("\t\t");
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write("║  ");
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("{0, 2}", counter);
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.Write(" ");
                Console.ForegroundColor = ConsoleColor.Gray;
                counter++;

                for (int j = 0; j < 11; j++)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("║  ");
                    Console.ForegroundColor = ConsoleColor.White;
                    if (j < 10)
                    {
                        if (player == 1)
                        {
                            Console.Write("{0,1}", BoatPosition[i, j]);
                        }
                        else if (player == 2)
                        {
                            Console.Write("{0,1}", BoatPosition2[i, j]);
                        }
                    }  
                    Console.ForegroundColor = ConsoleColor.DarkGray;
                    Console.Write("  ");
                    Console.ForegroundColor = ConsoleColor.White;
                }

                if (i < 9)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.Write("\n\t\t╠═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╬═════╣\n");
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
                
                if (i == 9)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine("\n\t\t╚═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╩═════╝\n");
                    Console.ForegroundColor = ConsoleColor.Gray;
                }          

            }
            _menu.PlayerTurn(player);
        }
    }
    
}
