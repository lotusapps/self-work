﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissorsWithInterfaces
{
    public enum GameResult
    {
        Win,
        Tie,
        Loss
    }
}
