﻿using System;
using System.Data.Common;
using System.Linq;

namespace LINQ
{
    class Program
    {
        /* Practice your LINQ!
         * You can use the methods in Data Loader to load products, customers, and some sample numbers
         * 
         * NumbersA, NumbersB, and NumbersC contain some ints
         * 
         * The product data is flat, with just product information
         * 
         * The customer data is hierarchical as customers have zero to many orders
         */
        static void Main()
        {
            //Exercise01();
            //Exercise02();
            //Exercise03();
            //Exercise04();
            //Exercise05();
            //Exercise06();
            //Exercise07();
            //Exercise08();
            //Exercise09();
            //Exercise10();
            //Exercise11();
            //Exercise12();
            //Exercise13();
            //Exercise14();
            //Exercise15();
            //Exercise16();
            //Exercise17();
            //Exercise18();
            //Exercise19();
            //Exercise20();
            //Exercise21();
            //Exercise22();
            //Exercise23();
            //Exercise24();
            //Exercise25();
            //Exercise26();
            //Exercise27();
            //Exercise28();
            //Exercise29();
            //Exercise30();
            //Exercise31();
            //Exercise32();
            //Exercise33();
            //Exercise34();
            //Exercise35(); 
            //Exercise36();
            //Exercise37();
            //Exercise38();
            //Exercise39();
            Exercise40();

            Console.ReadLine();
        }

        // 1. Find all products that are out of stock.
        private static void Exercise01()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(p => p.UnitsInStock == 0);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        // 2. Find all products that are in stock and cost more than 3.00 per unit.
        private static void Exercise02()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(p => p.UnitsInStock > 0 && p.UnitPrice > 3);

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName +  ", " + product.UnitPrice);
            }
        }

        // 3. Find all customers in Washington, print their name then their orders. (Region == "WA")
        private static void Exercise03()
        {
            var customers = DataLoader.LoadCustomers();

            var results = customers.Where(c => c.Region == "WA");

            foreach (var customer in results)
            {
                Console.WriteLine(customer.CompanyName);

                for (int i = 0; i < customer.Orders.Length; i++)
                {
                    Console.WriteLine("ID - " + customer.Orders[i].OrderID + " Total - " + customer.Orders[i].Total.ToString("C"));
                } 
            }
        }

        // 4. Create a new sequence with just the names of the products.
        private static void Exercise04()
        {
            var products = DataLoader.LoadProducts();

            var orderedProduct = from p in products
                orderby p.ProductName
                select p.ProductName;

            foreach (var o in orderedProduct)
            {
                Console.WriteLine(o);
            }
        }

        //5. Create a new sequence of products and unit prices where the unit prices are increased by 25%.
        private static void Exercise05()
        {
            var products = DataLoader.LoadProducts();

            var priceIncreased = from p in products
                select new { value = p.ProductName + " - " + 
                            "OP: " + p.UnitPrice.ToString("C") + " Price Increased: " + 
                            (p.UnitPrice + (p.UnitPrice * .25m)).ToString("C")};

            foreach (var i in priceIncreased)
            {
                Console.WriteLine(i.value);
            }
        }

        //6. Create a new sequence of just product names in all upper case.
        private static void Exercise06()
        {
            var products = DataLoader.LoadProducts();

            var UpperCase = from p in products
                select new {value = p.ProductName.ToUpper()};

            foreach (var p in UpperCase)
            {
                Console.WriteLine(p.value);
            }
        }

        //7. Create a new sequence with products with even numbers of units in stock.
        private static void Exercise07()
        {
            var products = DataLoader.LoadProducts();

            var evenUnits = products.Where(p => p.UnitsInStock%2 == 0);

            foreach (var item in evenUnits)
            {
                Console.WriteLine(item.ProductName +  " " + item.UnitsInStock);
            }
        }

        //8. Create a new sequence of products with ProductName, Category, and rename UnitPrice to Price.
        private static void Exercise08()
        {
            var products = DataLoader.LoadProducts();

            var seq = from p in products
                      let Price = p.UnitPrice
                      select new { value = p.ProductName + ": " + p.Category + " " + Price.ToString("C")};
        
            foreach (var prod in seq)
            {
                Console.WriteLine(prod.value);
            }
        }

        //9. Make a query that returns all pairs of numbers from both arrays such that 
        //the number from numbersB is less than the number from numbersC.
        private static void Exercise09()
        {
            var numberB = DataLoader.NumbersB;
            var numberC = DataLoader.NumbersC;

            var numBb = numberB.Select((numB, index) => new {B = numB, i = index});
            var numCc = numberC.Select((numC, indexB) => new {C = numC, i2 = indexB});

            var combined = from b in numBb
                from c in numCc
                where b.B < c.C && b.i == c.i2
                select new {value = b.B + " " + c.C};
                           
 
            foreach (var c in combined)
            {
                Console.WriteLine(c.value);
            }

        }

        //10. Select CustomerID, OrderID, and Total where the order total is less than 500.00.
        private static void Exercise10()
        {
            var customers = DataLoader.LoadCustomers();

            var orders = from c in customers
                from t in c.Orders
                where t.Total < 500
                select new {value = c.CustomerID + ": " + t.OrderID + " - " + t.Total.ToString("C")};

            foreach (var order in orders)
            {
                Console.WriteLine(order.value);
            }   
        }

        //11. Write a query to take only the first 3 elements from NumbersA.
        private static void Exercise11()
        {
            var numbers = DataLoader.NumbersA;

            var take3 = numbers.Take(3);

            foreach (var num in take3)
            {
                Console.WriteLine(num);
            }
        }

        //12. Get only the first 3 orders from customers in Washington.
        private static void Exercise12()
        {
            var customers = DataLoader.LoadCustomers();

            var orders = from customer in customers
                where customer.Region == "WA"
                from order in customer.Orders.Take(3)
                group order by customer.CompanyName;
      
            foreach (var customer in orders)
            {
                Console.WriteLine(customer.Key);
                foreach (var order in customer)
                {
                    Console.WriteLine("{0}: {1}",order.OrderID, order.Total.ToString("C"));
                }   
            }
        }

        //13. Skip the first 3 elements of NumbersA.
        private static void Exercise13()
        {
            var numbers = DataLoader.NumbersA;

            var Skip3Number = numbers.Skip(3);
            foreach (var num in Skip3Number)
            {
                Console.WriteLine(num);
            }
        }

        //14. Get all except the first two orders from customers in Washington.
        private static void Exercise14()
        {
            var customers = DataLoader.LoadCustomers();

            var SkipFirstTwoOrder = from customer in customers
                where customer.Region == "WA"
                from orders in customer.Orders.Skip(2)
                group orders by customer.CompanyName;

            foreach (var customer in SkipFirstTwoOrder)
            {
                Console.WriteLine(customer.Key);
                foreach (var order in customer)
                {
                    Console.WriteLine("{0}: {1}", order.OrderID, order.Total.ToString("C"));
                }
            }
        }

        //15. Get all the elements in NumbersC from the beginning until an element is greater or equal to 6.
        private static void Exercise15()
        {
            var numbers = DataLoader.NumbersC;

            var until6 = numbers.TakeWhile(num => num <= 6);

            foreach (var num in until6)
            {
                Console.WriteLine(num);
            }

        }

        //16. Return elements starting from the beginning of 
        //NumbersC until a number is hit that is less than its position in the array.
        private static void Exercise16()
        {
            var numbers = DataLoader.NumbersC;

            var number = numbers.Select((num, index) => new { numbers = num, Index = index })
                .TakeWhile(n => n.numbers > n.Index);

            foreach (var num in number)
            {
                Console.WriteLine(num);
            }
            
        }

        //17. Return elements from NumbersC starting from the first element divisible by 3.
        private static void Exercise17()
        {
            var numbers = DataLoader.NumbersC;

            var result = numbers.Where(n => n%3 == 0 && n > 0);

            foreach (var num in result)
            {
                Console.WriteLine(num);
            }
        }

        //18. Order products alphabetically by name.
        private static void Exercise18()
        {
            var products = DataLoader.LoadProducts();

            var result = from product in products
                orderby product.ProductName
                select product.ProductName;

            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
        }

        //19. Order products by UnitsInStock descending.
        private static void Exercise19()
        {
            var products = DataLoader.LoadProducts();

            //var result = from product in products
            //    orderby product.UnitsInStock descending
            //    select new {product.ProductName, product.UnitsInStock};

            var result =
                products.OrderByDescending(p => p.UnitsInStock)
                    .Select(p => new {name = p.ProductName, stock = p.UnitsInStock});

            foreach (var item in result)
            {
                Console.WriteLine("{0}: {1}", item.name, item.stock);
            }
        }

        //20. Sort the list of products, first by category, and then by unit price, from highest to lowest.
        private static void Exercise20()
        {
            var products = DataLoader.LoadProducts();

            var result = from product in products
                orderby product.UnitPrice descending
                group product by product.Category;

            foreach (var item in result)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(item.Key);
                Console.ForegroundColor = ConsoleColor.White;
                foreach (var product in item)
                {
                    Console.WriteLine("{0}: {1}", product.ProductName, product.UnitPrice.ToString("C"));
                }
            }
        }

        //21. Reverse NumbersC.
        private static void Exercise21()
        {
            var numbers = DataLoader.NumbersC;

            var result = numbers.Reverse();

            foreach (var num in result)
            {
                Console.WriteLine(num);
            }
        }

        //22. Display the elements of NumbersC grouped by their remainder when divided by 5.
        private static void Exercise22()
        {
            var numbers = DataLoader.NumbersC;

            var result = from number in numbers
                group number by number%5
                into remainder
                select (remainder);

            foreach (var remain in result)
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine(remain.Key);
                Console.ForegroundColor = ConsoleColor.White;

                foreach (var num in remain)
                {
                    Console.WriteLine(num);
                }
            }
        }

        //23. Display products by Category.
        private static void Exercise23()
        {
            var products = DataLoader.LoadProducts();

            var result = products.GroupBy(p => p.Category);

            foreach (var category in result)
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine(category.Key);
                Console.ForegroundColor = ConsoleColor.White;
                foreach (var item in category)
                {
                    Console.WriteLine(item.ProductName);
                }
            }
        }

        //24. Group customer orders by year, then by month.
        private static void Exercise24()
        {
            var customers = DataLoader.LoadCustomers();


            var orderDate = from customer in customers
                from order in customer.Orders
                orderby order.OrderDate.Month
                group order by order.OrderDate.Year
                into grp
                orderby grp.Key
                select grp;

            foreach (var date in orderDate)
            {
                Console.WriteLine(date.Key);

                foreach (var month in date)
                {
                    Console.WriteLine(month.OrderDate);
                }

            }
        }

        //25. Create a list of unique product category names.
        private static void Exercise25()
        {
            var products = DataLoader.LoadProducts();

            var category = products.Select(p => p.Category).Distinct().ToList();

            foreach (var item in category)
            {
                Console.WriteLine(item);
            }

        }

        //26. Get a list of unique values from NumbersA and NumbersB.
        private static void Exercise26()
        {
            var numbersA = DataLoader.NumbersA;
            var numbersB = DataLoader.NumbersB;

            var combined = numbersA.Concat(numbersB).Distinct().ToList();

            foreach (var num in combined)
            {
                Console.WriteLine(num);
            }
        }

        //27. Get a list of the shared values from NumbersA and NumbersB.
        private static void Exercise27()
        {
            var numbersA = DataLoader.NumbersA;
            var numbersB = DataLoader.NumbersB;

            //var combined = from numA in numbersA.ToList()
            //    from numB in numbersB.ToList()
            //    where numA == numB
            //    select numA;

            var combined = numbersA.Intersect(numbersB).ToList();

            foreach (var num in combined)
            {
                Console.WriteLine(num);
            }
        }

        //28. Get a list of values in NumbersA that are not also in NumbersB.
        private static void Exercise28()
        {
            var numbersA = DataLoader.NumbersA;
            var numbersB = DataLoader.NumbersB;

            var combined = numbersA.Except(numbersB).ToList();

            foreach (var num in combined)
            {
                Console.WriteLine(num);
            }
        }

        //29. Select only the first product with ProductID = 12 (not a list).
        private static void Exercise29()
        {
            var products = DataLoader.LoadProducts();

            var result = products.First(p => p.ProductID == 12);

            Console.WriteLine(result.ProductName);
        }

        //30. Write code to check if ProductID 789 exists.
        private static void Exercise30()
        {
            var products = DataLoader.LoadProducts();

            var result = products.Select(p => p.ProductID).Contains(789);

            Console.WriteLine(result);
        }

        //31. Get a list of categories that have at least one product out of stock.
        private static void Exercise31()
        {
            var products = DataLoader.LoadProducts();

            var result = products.GroupBy(p => p.Category)
                .Where(g => g.Any(p => p.UnitsInStock == 0))
                .Select(h => h.Key).ToList();

            foreach (var item in result)
            {
                Console.WriteLine(item);
            }

        }

        //32. Determine if NumbersB contains only numbers less than 9.
        private static void Exercise32()
        {
            var numbersB = DataLoader.NumbersB;

            var result = numbersB.All(n => n < 9);

            Console.WriteLine(result);

        }

        //33. Get a grouped a list of products only for categories that have all of their products in stock.
        private static void Exercise33()
        {
            var products = DataLoader.LoadProducts();

            var result = products.GroupBy(p => p.Category)
                .Where(g => g.All(p => p.UnitsInStock > 0)).ToList();

            foreach (var item in result)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(item.Key);
                Console.ForegroundColor = ConsoleColor.White;
                foreach (var product in item)
                {
                    Console.WriteLine("{0}: {1}", product.ProductName, product.UnitsInStock);
                }
            }
        }

        //34. Count the number of odds in NumbersA.
        private static void Exercise34()
        {
            var numbers = DataLoader.NumbersA;

            var results = numbers.Count(n => n%2 != 0);

            Console.WriteLine(results);
        }

        //35. Display a list of CustomerIDs and only the count of their orders.
        private static void Exercise35()
        {
            var customers = DataLoader.LoadCustomers();

            var results = customers.Select(customer => new {cust = customer.CompanyName, count = customer.Orders.Length});

            foreach (var item in results)
            {
                Console.WriteLine("{0}: {1}", item.cust, item.count);
            }
        }

        //36. Display a list of categories and the count of their products.
        private static void Exercise36()
        {
            var products = DataLoader.LoadProducts();

            var results = from product in products
                          group product.ProductName by product.Category
                into grp
                          select grp;

            // var results = products.GroupBy(g => g.Category).Select(p => p.Where(p));

            foreach (var item in results)
            {
                Console.WriteLine(item.Key);
                foreach (var i in item)
                {
                    Console.WriteLine(i.Count());
                }
            }

        }

        //37. Display the total units in stock for each category.
        private static void Exercise37()
        {
            var products = DataLoader.LoadProducts();
           
            var result = products.GroupBy(p => p.Category)
                .Select(g => new {sum =  g.Sum(i => i.UnitsInStock), g.Key});
           
            foreach (var item in result)
            {
                Console.WriteLine(item.Key);
                Console.WriteLine(item.sum);
            }
        }

        //38. Display the lowest priced product in each category.
        private static void Exercise38()
        {
            var products = DataLoader.LoadProducts();

            var result = products.GroupBy(p => p.Category)
                .Select(u => new {min = u.Min(i => i.UnitPrice), u.Key});

            foreach (var p in result)
            {
                Console.WriteLine("{0}: {1:c}", p.Key, p.min);
            }
        }

        //39. Display the highest priced product in each category.
        private static void Exercise39()
        {
            var products = DataLoader.LoadProducts();

            var result = products.GroupBy(p => p.Category)
                .Select(u => new { min = u.Max(i => i.UnitPrice), u.Key });

            foreach (var p in result)
            {
                Console.WriteLine("{0}: {1:c}", p.Key, p.min);
            }
        }

        //40. Show the average price of product for each category.
        private static void Exercise40()
        {
            var products = DataLoader.LoadProducts();

            var result = products.GroupBy(p => p.Category)
                .Select(u => new { min = u.Average(i => i.UnitPrice), u.Key });

            foreach (var p in result)
            {
                Console.WriteLine("{0}: {1:c}", p.Key, p.min);
            }
        }
    }
}
